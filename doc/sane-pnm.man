.TH sane-pnm 5 "21 April 1997"
.IX sane-pnm
.SH NAME
sane-pnm - SANE PNM image reader pseudo-backend
.SH DESCRIPTION
The
.B sane-pnm
library implements a SANE (Scanner Access Now Easy) backend that
provides access to PNM (Portable aNyMap files, which covers PBM bitmap
files, PGM grayscale files, and PPM pixmap files).  The purpose of
this backend is primarly to aide in debugging of SANE frontends.  It
also serves as an illustrative example of a minimal SANE backend.
.SH "DEVICE NAMES"
This backend provides two devices called
.B 0
and
.BR 1.
.SH CONFIGURATION
No configuration required.
.SH FILES
.TP
.I @LIBDIR@/libsane-pnm.a
The static library implementing this backend.
.TP
.I @LIBDIR@/libsane-pnm.so
The shared library implementing this backend (present on systems that
support dynamic loading).
.SH ENVIRONMENT
.TP
.B SANE_DEBUG_PNM
If the library was compiled with debug support enabled, this
environment variable controls the debug level for this backend.  E.g.,
a value of 128 requests all debug output to be printed.  Smaller
levels reduce verbosity.
.SH AUTHOR
Andreas Beck, Gordon Matzigkeit, and David Mosberger
