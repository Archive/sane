-*-Mode: outline-*-

New with 1.00:

* Upgraded to libtool-1.2.
    This reportedly gets SANE to build on Solaris versions with a
    broken printf.

* saned
    Matching of hostnames is no longer case-sensitive.

* New Abaton backend (by David Huggins-Daines)
    Supports "Scan 300/GS" and may work with "Scan 300/S" but the
    latter is untested.

* New Agfa Focus backend (by Karl Anders �ygard)
    Supports:
     o AGFA Focus GS               (6 bit gray scale)     (untested)
     o AGFA Focus Lineart          (lineart)              (untested)
     o AGFA Focus II               (8 bit gray scale)     (untested)
     o Siemens S9036               (8 bit gray scale)     (untested)
     o AGFA Focus Color            (24 bit color 3-pass)
     o AGFA Focus Color Plus       (24 bit color 3-pass)

* New Kodak DC210 still camera backend (by Brian J. Murrell)

* New Ricoh backend (by Feico Dillema).

* New HP backend.
  The backend formerly known as "xhp" is now the default HP backend.
  This backend should support HP scanners much better and was
  contributed by Geoffrey Dairiki and Peter Kirchgessner.

  - Added support for HP 6200C
  - Suppress halftone mode on photosmart (it is not supported)
  - Show calibrate button on photoscanner only for print media
  - Add media selection for photoscanner
  - Cleanup hp_scsi_s structure

* Updated apple backend (by Milon Firikis).
  Now you should be able to scan from ColorOneScanners (in non color
  modes) and maybe from OneScanners (untested).

* Updated Artec backend (by Chris Pinkham).

* Updated Kodak DC25 backend (by Peter Fales).

* Updated Microtek backend (by Matto Marjanovic).
  - Fix segfault on exit due to unnecessary free() in sane_close().
  - Fix to red channel shift bug (which crept in during rewrite of
    color-handling code). 
  - Addition of "patch level" to version codes. 

* Updated Microtek2 backend
  - Added support for ScanMaker 330, ScanMaker 636, ScanMaker E3plus,
    ScanMaker X6 and Phantom 636.
  - Other improvements (this includes support for automatic document feeders
    and transparency adapters, missing option descriptions were added).
  - Updated the manual page.

* Updated Mustek backend (patches by Andreas Bolsch and Heiko Schroeder)

  - Heiko's patch should make resolutions >300dpi work for MFS-6000CX.

  Andreas's patches:

  - Should work with ScanExpress 6000SP, 12000SP as well as older models
  - Bug with MFS-12000SP corrected (poined out by Daniel Deckers)
  - Bug which caused xscanimage to crash after color preview corrected
  - Improvement(?) in high resolution

  Important Notes for ScanExpress models:

  - Resolutions below 60 dpi don't work (firmware problem).
  - Resolutions >300 dpi (6000 SP) or >600 dpi (12000 SP) result in
    different x/y-resolution as 6000 SP and 12000 SP have in fact only
      300 dpi and 600 dpi CCD sensors, respectively.
  - Resolutions >300dpi in color mode sometimes result in corrupted images
    (parts duplicated/shifted *HORIZONTALLY*) depending on hardware
    configuration. Killing all other applications and disabling swap
    (if sufficient physical memory available) may help. 
    I added some code which writes to every page of the buffer prior 
    to scanning to fool the memory management and scanned a full page
    color scan with 600dpi * 1200dpi. Very slow but image seemed ok
    after pnmscale.  
  - Max. scan area:  0-216mm (hor.), 2.5-294.5mm (ver.)
    The scanners can't scan the first 2.5mm (ver.), so you *MUST*
    specify the scan area according to this restriction!
  - The scanners support only lineart (1 bpp), gray (8 bpp), color (24 bpp).
    Although the scanners have 10 bit (6000 SP) or 12 bit (12000 SP) 
    A/D-converters, they can send only 8 bit values to the host.
    The higher resolution may only be used via gamma table.
  - For compatibility only 256 entry gamma tables may be specified, the
    actual gamma table sent to the scanner is linearly interpolated from it.
  - In lineart mode only brightness may be adjusted. 
  - Cover open/closed reported only via debug output and ignored otherwise.
  - Use of SCSI card supplied with scanner is *NOT* recommended. (Don't
    forget termination!)
  
* Updated UMAX backend (by Oliver Rauch)
  - added output of umax-backend-version in sane_init
  - added Linotype Hell Jade2 (Vobis ScanBoostar Premium)
    SCSI-ID "LinoHell","Office2" as supported scanner
  - changed base of calculation of scanwidth and scanlegth from
    pixel to resolutionbase (1/300, 1/600 or 1/1200 inch)
  - changed calculation for width in pixels for lineart mode
  - changed selection for calibration mode
  - added inquiry for UMUX UC1200SE
  - corrected 12/36-bit-mode in send_gamma_data and array in umax.h

* Updated SnapScan backend to v0.5 (by Kevin Charter)
  - bug fixes (Wolfgang, David)
  - addition of threshold control for lineart mode (Mikko)
  - Vuego 310S support (Wolfgang)
  - default scan area now maximal (Kevin)

New with 0.74:

* GIMP auto-detection should work again.

* Service name for remote scanning changed from "saned" to "sane".
  Be sure to update your /etc/services and /etc/inetd.conf if you
  use remote scanning!  We generally try to avoid such incompatible
  changes, but the name "saned" was definitely wrong, so it seemed
  better to change it now.

* Thanks to Matto Marjanovic work, each backend now comes with a
  .desc file that describes exactly what devices it supports.  These
  description files can be automatically translated into various
  other formats, such as HTML.  See:

	http://www.mostang.com/sane/sane-backends.html

  for an example as to what this can do for you.

* New backend for Kodak DC25 digital cameras (by Peter Fales).

* Updated Artec backend (by Chris Pinkham).

* Updated Microtek backend (by Matthew Marjanovic)

 o Complete rewrite of bit-shuffling, buffering, and color-handling code.
 o Improved rescaling algorithm for expanded resolution scans.
 o Support for 600GS (and maybe ZS, too) (thanks to Oliver Neukum).
 o Support for document autofeeder and IIG (thanks to Ross Crawford).
 o Fixed sane_cancel.
 o sane_get_parameters size estimates are now accurate to the last bit.
 o get_scan_status timeout increased (for 600GS), status code fixed.
 o Fixed parsing of 3-pass bit in INQUIRY data.
 o Stopped sending gamma tables to scanners that don't want them.
 o Made precalibration a bit more robust (always attempt at least one
   scan line now).
 o Much, much code clean-up.
 o Tested & working with saned.  (Atrocious hack so sane_read
   handles buffers smaller than one scanline.)
 o Auto-expand pre/post hold buffers as necessary (fixes some problems
   with single-pass color scans).
 o Added configuration file option to disable precalibration logic.
 o Fixed document size calculations.
 o Added more informative scsi-II note.
 o Remove misnomer "Highscan" from manpage.
 o Updated man-page.

* Updated Microtek2 backend (by Bernd Schroeder)

 o changed the code for gamma calculation and added a custom gamma table
   to the options. In some cases this requires an additional memcpy(),
   but the code is now leaner and cleaner.
 o fixed the bugs that the backend didn't compile with non gcc compilers.
 o added an option to control exposure times.
 o rewrote the code that processes the data that is received from the 
   scanner and transfers it into the frontend buffer. The smallest unit of
   data that can be copied into the frontend buffer is now a pixel, no
   longer a complete line.
 o added (a yet) undocumented option "strip-height" that allows to control
   the number of lines that is read from the scanner with one "read image".
 o fixed a bug that caused scanimage to sigsegv when calling sane_exit() 
   without having written all data into the output file.
 o added code to support scanners with more than 8 bit output per color.
   Due to the lack of such a scanner this functionality is COMPLETELY UNTESTED
   and there are some potential sources of bugs, but at least one could give
   it try.
 o added sanei_config_attach_matching_devices()
 o improved the code for the check if it really is Microtek SCSI-II scanner
 o fixed the "lineart inverted" bug
 o The threshold option in lineart mode is now in the enhancement group
 o changed the default for the resolution
 o The values for the SANE_Params struct are calculated more precisely
 o dito the number of scan lines that fit into the frontend buffer
 o changed some return stati ( replaced SANE_STATUS_INVAL with
   SANE_STATUS_IO_ERROR where the first one is not allowed)
 o completely rewrote the end of scan and cancel scan handling
 o fixed another bug that caused xscanimage to crash with sigsegv
   under some circumstances (check of the inquiry result was wrong)
 o added model code for a Vobis Highscan
 o support for new format of configuration file (including
   "option <opt> <val>")

* Updated Nikon Coolscan backend (by Didier Carlier).

* Updated UMAX backend (by Oliver Rauch):
  o Cancelling a scan now works, cancelling a preview still makes problems! 
  o Preview fix is activated for Astra 600S, Astra 610S and Vista S6E
    scanners. (calibration by driver still does not work for these scanners). 
     - removed button for preview patch 
  o Quality calibration / calibration by driver now works for same scanners. 
     - added selection for type of shading data calculation 
  o Solved problem with black area at top of the image 
    (happend only with some SCSI-adapters). 
  o Added gamma download format type 0 and type 1 for older scanners. 
  o Added Astra 1220S as supported scanner: 
     - added 36 gamma input bits support for Astra 1220S 
     - added 36 output bits support, but there is no frontend that can
       handle it! 
  o Added inquiry for Escom Image Scanner 256 (UMAX UG80). 

* OS/2 (by Yuri Dario):
  o Updated SCSI support so sanei_scsi_find_devices is supported now.
  o Epson parallel support added (by Rocco Caputo <troc@netrus.net>)

New with 0.73:

* In xscanimage, the selection box now gets updated again when changing
  the geometry option sliders---really!

* On Linux, it is now possible to specify SCSI device names by entering
  their SCSI ids.  For example, instead of "/dev/scanner", one can now
  specify "scsi MUSTEK" to get all scanners manufactured by Mustek.  This
  feature is currently supported by the all SCSI backends except
  microtek2.  This feature is courtesy of Oliver Rauch.

* Backend libraries are now installed in $(libdir)/sane (/usr/local/lib/sane/,
  by default).

* Updated Microtek backend (Matto Marjanovic):
	- ScanMaker 600ZS, Agfa Arcus II, StudioScan, and StudioScan II
	  now supported (or recognized, at least ;-).
	- Fixed 3-pass scanning.
	- Various bug-fixes (see ChangeLog for details).

* New Microtek2 backend (Bernd Schroeder)
  This backend supports the ScanMaker 630 and possibly other newer scanners
  that are not supported by the "old" Microtek backend.

  Additional info by Bernd:

  This is the very first release of this backend, so consider this
  software to be in alpha state. The backend was developed on a Pentium
  (60 Mhz) with Linux 2.0.29 and a ScanMaker 630 attached to the
  Adaptec AHA1505E that shipped with the scanner.  As frontend
  xscanimage was used.  It is intended to work with other models, too,
  but there are no experiences yet.

  The following options are supported:

       - 8-bit color, 8-bit grayscale, halftone and lineart scans.
       - brightness, contrast, shadow, midtone and highlight control
       - scalar gamma correction.

  Options that are not yet supported include:
   
       - 3-pass scanners
       - more than 8 bit per color output data. Provisions are made here
         and there in the code, to support more than 8 bit, but that's
         uncomplete.

* configure --disable-static should now work as expected.

New with 0.72:

* New backend for Artec scanners.
   This backend is known to work with Artec AT3, but others may work
   too (A6000C should work).

* Updated DMC and Microtek backend.

* Updated UMAX backend:
** added exposure time adjustment (for high-end scanners)
** added lamp density control (for high-end scanners)
** UMAX Astra 6X0S works in color-mode now, you have to enable
   preview_patch!
** added support for UMAX UC1200S and UC1260, but it will not work fine!
** enabled x-resolution greater than 600 dpi if supported by scanner
   (e.g. UMAX S12) but it sometimes still does not work right!

* Updated SnapScan backend:

There is now support for the SnapScan 310 and 600 scanner models.

* OS/2 and FreeBSD support should be working again.

* New backend writer's guide to SANE
   File backend/GUIDE outlines a few rules that should help in writing
   a new backend.

New with 0.71:

* Polaroid Digital Microscope Camera (DMC) backend
  Written by David Skoll <dskoll@chipworks.com>.

* Apple scanner backend
  Written by Milon Firikis <milonf@isosun.ariadne-t.gr>.  This backend
  supports AppleScanner and has preliminary support for OneScanner and
  ColorOneScanner.

* Nikon CoolScan backend
  Written by Didier Carlier <didier@sema.be>.

* Apollo Domain/OS support
  Contributed by Paul Walker <paul@uugw.hifn.com>.

New with 0.70:

* Preliminary version of AGFA SnapScan backend is now included.
  This backend is know to work with AGFA SnapScan scanners but
  is unlikely to work with any other AGFA
  See scanner.http://www.cs.ualberta.ca/~charter/snapscan.html
  for details.

* Various minor bug fixes that prevented SANE from building on
  non-Linux platforms.

* xscanimage now honors WM_DELETE message.

* Updated UMAX backend.


New between 0.6 and 0.69:

* Mustek backend now supports the Transparency Adapter on the Paragon 1200SP
  (MFS-12000SP).

* New backend for Canon scanners.
  This backend was written by Helmut Koeberle <helmut.koeberle@bytec.de>.
  It is known to work with the CanonScan 600 though not all features
  are supported yet.

* Solaris SCSI support now exists.
  Thanks to Martin Huber <hu@garfield.m.isar.de>, the SCSI backends are
  now usable under Solaris, too.  See README.solaris for details.

* AIX SCSI support now exists.
  Thanks to Fred Hucht & Michael Staats, the SCSI backends are now usable
  under AIX, too.

* New backend for Tamarack and ESCOM scanners.
  This backend was written by Roger Wolff <R.E.Wolff@BitWizard.nl> of
  BitWizard.

* New backend for Siemens S9036 scanner.
  This backend was written by Ingo Schneider
  <schneidi@informatik.tu-muenchen.de>.

* find-scanner (by Oliver Rauch)
  SANE now comes with a program called find-scanner (in the tools
  subdirectory) that can be used to find the device name of attaches
  SCSI scanners.

  Note that this program is not normally installed as part of the
  normal SANE installation as this program is not something an
  end-user should ever have to use.

* The Mustek backend has preliminary support for the Paragon 600 II N
  scanner.  This scanner attaches directly to a Mustek-supplied
  ISA card which implements a funky parallel port.  For details, see
  the section entitled PARALLEL PORT SCANNERS in sane-mustek(5).
  Use at your own risk!

* The location of the configuration files can now be overridden with
  environment variable SANE_CONFIG_DIR (see also man-pages for the
  backends and programs).

* When preloading backends into dll, they now appear in the same order
  as if they had been loaded dynamically (i.e., in reverse order in
  which they're listed in dll.conf).

* Java frontend (by Jeff Freedman)
  SANE now includes a Java frontend for SANE.  However, the Java files
  are not built by default.  See japi/README.JAVA for details.

* There is a Java API for SANE now.  See japi/README.JAVA for details.
  This code is courtesy of Jeff Freedman <jsf@hevanet.com>.

* UMAX updates (by Oliver Rauch):

   - the umax backend is now fully runtime configuable---there are no
     longer any build-time configuration options.

   - Umax T630, Astra 610S, and Linotype Hell Office scanners are now
     supported

   - gamma-data now works on Astra 1200 S with 30 bits/pixel

     Note: None of the SANE frontends presently support 30 bits/pixel.
	   If you're interested in fixing this, send mail to
	   sane-devel@mostang.com.

* The Mustek backend is now fully runtime configurable---there are no
longer any build-time configuration options.  To this end, the
mustek.conf configuration file now supports options linedistance-fix,
lineart-fix, and strip-height (see sane-mustek(5) for details).

* New backend for Epson scanners

An alpha-quality backend for Epson scanners is now included with SANE
thanks to the efforts of Kazuhiro Sasayama <kaz@hypercore.co.jp>.

* OS/2 Support

Thanks to Jeff Freedman <jsf@hevanet.com> SANE now supports OS/2.

* New backend for Microtek scanners

Thanks to the excellent work of Matthew Marjanovic <maddog@mir.com>,
the Microtek is now taking shape.

* Irix SCSI support

Thanks to the work of Michael Sweet <mike@easysw.com>, there is now
SCSI support for Irix!

* Improvements to the UMAX backend (by Oliver Rauch):
** workaround for preview-bit-problem in RGB-mode (UMAX S6E ...)
** unsupported options are disabled
** now three_pass_scan should work
** new supported scanners:
*** UC840
*** Astra 1200S

* The Mustek configuration file (mustek.conf) now supports a configuration
  option to limit the height of the strip that is scanned with a single
  SCSI read command.  The syntax is:

	option strip-height HEIGHT

  where HEIGHT is a floating point number that gives the maximum strip height
  in inches.  This option should be set to a small value (e.g., 1
  inch) when the scanner is connected to a SCSI bus shared with other devices
  or when using a broken SCSI driver whose timeouts expire prematurely.  For
  best scan performance, the strip-height should be set to a large value or
  the option should be removed completely.  See the sane-scsi(5) man-page for
  details on how drivers with premature timeouts can be fixed (the Linux
  ncr810 driver is the only known driver with this problem at this point).

* The preview window now properly draws the initial window-selection.

* Mustek backend now uses a SCSI command queue to avoid performance
  problems with three pass scanners.  This can reduce scantimes from
  15 minutes down to 3 minutes!

* Mustek backend will now wait for up to 1 minute for a scanner to
  become ready if the scanner name is specified explicitly.  E.g.,
  "scanimage -d mustek" will timeout almost right away (since the
  Mustek device name is not specified explicitly) whereas
  "scanimage -d mustek:/dev/scanner" will wait for up to a minute.

* HP backend now uses pixel-unit commands to accommodate ScanJet 5P.

* Platform-specific SCSI setup info is now in sanei-scsi(5).

* xscanimage(1) now has a section on how to run it under GIMP.

* B&W qcam support should now work (reports on how well it works are
  welcome).

* Exiting xscanimage with preview window open should no longer cause
  an error.

* Support for OpenStep/NeXTStep added (xscanimage and xcam require an
  X server and the GTK+ libraries, though).  User-level SCSI is
  supported.

* SCSI support for NetBSD and FreeBSD should work now.  Thanks to
  NOGAYA Shigeki <nogaya@mbox.kyoto-inet.or.jp> and
  Amancio Hasty <hasty@rah.star-gate.com> for relevant patches.

* New man-page sane-scsi(5) with platform-specific SCSI tips and tricks.

* SANE now builds on HP-UX (SCSI support untested) and IRIX (no SCSI
  support), too.

New in 0.6:

* UMAX scanners are now supported!  Kudos to Oliver Rauch
  <orauch@physik.uni-osnabrueck.de> and Michael K. Johnson
  <johnsonm@redhat.com>.

* scan got renamed to scanimage to avoid a nameclash with an MH program
  by the same name.  For consistency, xscan also got renamed to
  xscanimage.

* Man-pages!  There finally are at least a few man-pages.  At present,
  the following is covered:
	saned.1 scanimage.1 xscanimage.1
	sane-dll.5 sane-hp.5 sane-mustek.5 sane-net.5 sane-pint.5
	sane-pnm.5 sane-qcam.5 sane-umax.5

* SANE no longer insists on using GCC.  GCC works best, but other ANSI C
  compilers will now also produce usable executables.

* xscanimage now supports vector options (such as gamma-tables which
  are also known as intensity or tonemaps).

* The gamma-table (intensity/tone-map) handling changed in the Mustek
  backend.  As a result, when using scanimage it is now necessary to
  specify option --custom-gamma before gamma-tables can be specified.
  Also, the handling of the intensity table is now handled better in
  color mode (it no longer overrides the color tables; instead
  the composition of the intensity and the color channel table is
  used).

* The SANE header files are now isolated in the include/sane directory
  and those files get now installed as part of "make install".  Thanks
  to Michael K. Johnson <johnsonm@redhat.com> for this patch!

* xscanimage now displays the options' documentation strings as
  tooltips (can be disabled via the "Preferences" menu).

* scanimage now supports three-pass scanning and scanning of images whose
  height is not known a priori (e.g., hand-held scanners).

* The Mustek backend now supports an option to force preview scans to be
  monochrome (good to save time on three-pass scanners).

* configure can now be run from any directory, as it's supposed to (makes
  it easier to build SANE for multiple platforms from the same source
  tree).

* xcam and xscanimage should now build on Solaris, too (thanks to
  Tristan Tarrant).

* copyright info in various files have been adjusted.  See LICENSE for
  the general idea behind SANE licensing.

* Many, many bugfixes.

New in 0.5:

* The same xscan binary can now function as a standalone frontend or
  as a gimp extension.  If installed as a GIMP extension, xscan will
  attach itself as Extensions->Acquire Image.

* The pnm backend now has an option to simulate a three-pass scanner.
  Good for testing.

* xscan now supports previewing and (persistent) preferences.

* The build process should be much more robust now.  It requires GNU make
  and gcc but should be completely unproblematic otherwise.  A simple
  "configure" should result in a working SANE environment even on systems
  where dynamic loading is unavailable.  Various options are available
  to tailor the SANE setup.  See README for details.

* A first implementation of the HP backend now exists (for ScanJet
  scanners).

* A first implementation of the net backend and saned (network daemon)
  now exists.  So it is now possible to scan across the network!  See
  backend/net.README and frontend/saned.README for details.

* xcam, a camera frontend is now included.  See frontend/xcam.README for
  details.

* Renamed metadl to dll.

New in 0.4:

* A first implementation of the Connectix quickcam backend now exists.
  At present, only color cameras are known to work, though it shouldn't
  be too hard to get the b&w versions to work as well.

* Improvements for the command-line frontend scan:

** Option settings are now applied _before_ the help info is printed.
   This allows to see what the available options are with certain options
   in effect.
** It can now deal with SANE_INFO_RELOAD_OPTIONS.
** It now prints the current option values in the help message (except for
   vectors).


New in 0.33:

* sane_get_devices() now takes a second argument of boolean type.  If it's
  SANE_TRUE, then the backend must return local (non-remote) devices only.

* scan now uses the default-unit of `mm' (millimeters) again for lengths.
  Using `cm' (centimeter) proved confusing since the help messages print
  length values in millimeters.

* Debugging can now be controlled on a per-backend basis.  The debug-level
  of backend <be> is set by environment variable SANE_DEBUG_<be>.  For example,
  to see all debug messages of the metadl backend, set SANE_DEBUG_METADL to
  a large value (say 128).  The sanei.h file provides three macros
  to assist in using this debug facility: DBG_DECL to declare the integer
  variable that holds the debug level, DBG_INIT to initialize debugging,
  and DBG to print a debug message.  See backend/sanei.h and
  backend/metadl.c for details and examples.

* scan now supports setting options to "auto" mode (e.g., --brightness=auto
  would ask the backend to select brightness automatically if that option
  supports automatic mode

* scan now allows abbreviating the values of a string-lists.  Case is ignored
  and the best matches is used (either longest unique match or exact match
  when ignoring case)

New in 0.32:

* xscan improved much.  See frontend/xscan.README and frontend/xscan.BUGS
  for details.

New in 0.31:

* xscan has improved much.  See frontend/xscan.CHANGES for details.


New in 0.3:

* The location of the SANE configuration files moved from /etc/saneconf
  to ${prefix}/etc/sane.d.  This normally expands into /usr/local/etc/sane.d.

* Real build environment.  It's GNU autoconf based so all you should have
  to say is:

	./configure
	make
	make install
