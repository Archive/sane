set env LD_LIBRARY_PATH ../backend:../sanei:/usr/local/lib
set env LD_BIND_NOW 1
set env SANE_DEBUG_PNM 128
set env SANE_DEBUG_NET 128
set env SANE_DEBUG_DLL 2
set env SANE_DEBUG_MUSTEK 128
set env SANE_DEBUG_PINT 128
#set env SANE_DEBUG_QCAM 128
dir ../backend
