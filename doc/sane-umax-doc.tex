\documentstyle{article}
\parindent0pt
\parskip1ex
\oddsidemargin-8mm
\evensidemargin-12mm
\topmargin-30mm
\textheight25cm
\textwidth17.3cm
\input{epsf.tex}

\title{
 \begin{figure}[h]
  \centerline{\epsfxsize=8cm \epsffile{sane.ps}}
  \centerline{\epsfxsize=4cm \epsffile{umaxlogo.ps}}
 \end{figure}
 UMAX-backend for SANE - User's Guide }

\author{Oliver Rauch
{\tt <Oliver.Rauch@Wolfsburg.DE>}}
\date{17th october 1998}


\begin{document}

\maketitle


\newpage
\tableofcontents

\newpage
\section{User's Manual for umax-backend}

\subsection{Frontend functions and options:}

This backend dynamically enabels the options, scan-modes etc. that are
supported by the scanner in dependence of the scanning-mode and other options.


 \begin{figure}[h]
   \centerline{
   \epsfxsize=16cm
   \epsffile{sane-umax-window.ps}}
 \end{figure}


\subsubsection{Scan modes:}

The available scan modes depend on the scanner type!\\

\small
\begin{tabular}{|l|l|}
\hline
Mode&Remark\\
\hline
\hline
Lineart&1 bit/pixel black/white mode\\
Halftone&1 bit/pixel dithered black/white mode\\
Greyscale&8 / 10 / 12 bits/pixel grey mode\\
Color&24 / 30 / 36 bits/pixel RGB mode\\
\hline
\end{tabular}
\normalsize


\subsubsection{Scan sources:}

\begin{itemize}
\item Flatbed
\item Transparency adapter (UTA)
\item Automatic document feeder (ADF)
\end{itemize}
The UTA and the ADF are only selectable if they are available!


\subsubsection{Options:}

There are two kind of options: the standard options that are often used
and the \emph{advanced options}.\\

\small
\begin{tabular}{|l|l|}
\hline
Option&Remark\\
\hline
\hline
10-bit mode&Use 10 (30) bits/pixel instead of 8 (24) bits/pixel,\\
&only supported by Astra 1200S.\\
&(NOT SUPPORTED FROM ANY FRONTEND IN THE MOMENT!)\\
\hline
12-bit mode&Use 12 (36) bits/pixel instead of 8 (24) bits/pixel,\\
&only supported by Astra 1220S.\\
&(NOT SUPPORTED FROM ANY FRONTEND IN THE MOMENT!)\\
\hline
Analog gamma correction&For color/intensity enhancement, value-range: 1..2,\\
&doesn't reduce the number of used colors!\\
&only supported by some scanners (e.g. Supervista S12).\\
\hline
Bind RGB values&Use same values for each color, only selectable if such options are available\\
\hline
Brightness&Define brightness for halftone-mode.\\
\hline
Contrast&Define contrast for halftone-mode.\\
\hline
Custom gamma table&Digital gamma correction, for color/intensity enhancement!\\
&Side-effect: may reduce the number of used colors!\\
&For 30 and 36 bit scanners the used gamma table uses 30/36 input bits, so the\\
&reduction of the used colors is not so big!\\
\hline
Highlight&Define radiance level for highlight, only supported by some scanners.\\
\hline
Lamp warmup&Enable extended lamp warmup, only supported by some scanners.\\
\hline
Negative scan&Inverts color-intensity: for scanning negatives.\\
\hline
Quality calibration&Do quality white calibration.\\
\hline
Shadow&Define radiance level for shadow, only supported by some scanners.\\
\hline
Threshold&Define threshold for lineart-mode.\\
\hline
\end{tabular}
\normalsize


On graphical frontends the \emph{advanced options}
may be disabled so the frontend-window keeps small.
To enable the \emph{advanced options}
take a look at the frontend's manual!\\

\small
\begin{tabular}{|l|l|}
\hline
Advanced Option&Remark\\
\hline
\hline
Cal. exposure time&Define exposure time for calibration,\\
&only supported by some scanners.\\
\hline
Cal. lamp density&Define lamp density for calibration,\\
&only supported by some scanners.\\
\hline
Scan exposure time&Define exposure time for scan,\\
&only supported by some scanners.\\
\hline
Scan lamp density&Define lamp density for scan,\\
&only supported by some scanners.\\
\hline
Scan-speed&Slow speed makes bad noises    *** DON'T USE IT ***\\
\hline
Set exposure time&Enable selection of exposure time,\\
&if not enabled, scanner uses default values.\\
\hline
Set lamp density&Enable selection of lamp density, if not\\
&enabled, scanner automatically selects a value.\\
\hline
Set scan lamp density&Enable selection of lamp density for scan, if not enabled\\
&scanner uses the value that was used for calibration.\\
\hline
Shading type&Define type of calculation of shading data for quality calibration\\
\hline
Smearing&Don't know for what it is      *** DON'T USE IT ***\\
\hline
\end{tabular}
\normalsize



\newpage
\section{ Installation Manual}

\subsection{Compilation and Installation}

The umax-backend is created and installed automatically while compiling
and installing SANE.

\subsection{Configuration:}

The configuration file for the UMAX-backend resides in

\begin{verbatim}
     /usr/local/etc/sane.d/umax.conf.
\end{verbatim}

Its contents is a list of device names that correspond to UMAX scanners.
Empty lines and lines starting with a hash mark (\#) are ignored.
Lines starting with the word {\bf scsi} use variable device selection,
for further information take a look at sane-scsi.

A sample configuration file is shown below:

\begin{verbatim}
     #scsi vendor model type  bus channel id lun
     scsi  UMAX   *     *     *   *       *  *
     /dev/scanner
     # this is a comment
     /dev/sge
\end{verbatim}


The special device name must be a generic SCSI device or a symlink to such a
device. To find out to which device your scanner is assigned and how you
have to set the permissions of that device, have a look at sane-scsi or
use the tool {\tt find-scanner}.

The file may include devices, that correspond to scanners by other vendors.
The Umax-backend will ignore these devices if they are not supported by the
Umax-backend.


\subsection{SCSI adapter tips:}

The SCSI-adapters that are shipped with some Umax-scanners are not supported
by Linux and most other platforms, so you typically need to purchase another
SCSI-adapter that is supported by your platform. See the relevant hardware
FAQs and HOWTOs for your platform for more information.\\

The UMAX-scanners do block the scsi-bus for a few seconds while scanning.
It is not necessary to connect the scanner to its own SCSI-adapter.
But if you need short response time for your SCSI-harddisk (e.g. if your
computer is a file-server), I suggest you use an own SCSI-adapter for
your UMAX-scanner.\\

See also: sane-scsi




\subsection{Files:}

\begin{itemize}
\item The backend configuration file: /usr/local/etc/sane.d/umax.conf
\item The static library implementing this backend:

/usr/local/lib/libsane-umax.a
\item The shared library implementing this backend :

/usr/local/lib/libsane-umax.so
(present on systems that support dynamic loading)
\end{itemize}





\subsection{Environment:}



\begin{itemize}
\item SANE\_DEBUG\_UMAX
\item see sane-dll
\item see sane-scsi
\item see backends
\item see frontends
\end{itemize}


If the library was compiled with debug support enabled, this environment
variable controls the debug level for this backend. E.g., a value of 128
requests all debug output to be printed. Smaller levels reduce verbosity:




\subsubsection{SANE\_DEBUG\_UMAX values}

\small
\begin{tabular}{|l|l|}
\hline
Number&Remark\\
\hline
\hline
0&print important errors (printed each time)\\
1&print errors\\
2&print sense\\
3&print warnings\\
4&print scanner-inquiry\\
5&print informations\\
6&print less important informations\\
7&print called procedures\\
8&print reader-process messages\\
10&print called sane-init-routines\\
11&print called sane-procedures\\
12&print sane infos\\
13&print sane option-control messages\\
\hline
\end{tabular}
\normalsize


Example:

export SANE\_DEBUG\_UMAX=8




\newpage
\section{Supported scanners}

\bf{The umax-backend only supports SCSI-scanners, parallel-scanners are not supported!}
Here is a list of scanners that have been tested:\\

\small
\begin{tabular}{|l|l|l|c|c|c|c|c|c|c|}
\hline
Name&Vendor&SCSI-ID&F/W&Line-&Half-&Grey-&Color&Bits&Maximum\\
&&&Version&art&tone&scale&RGB&&resolution\\
\hline
\hline
UMAX Vista S6           &UMAX     &Vista-S6        &all    &ok    &--    &ok     &ok    &1,8,24    &  300 x  600\\
UMAX Vista S6E          &UMAX     &Vista-S6E       &all    &ok    &--    &ok     &ok    &1,8,24    &  300 x  600\\
UMAX Vista S6E          &UMAX     &UMAX S-6E       &all    &ok    &--    &ok     &ok    &1,8,24    &  300 x  600\\
UMAX Vista S6E          &UMAX     &UMAX S-6EG      &all    &ok    &--    &ok     &ok    &1,8,24    &  300 x  600\\
UMAX Vista S8           &UMAX     &Vista-S8        &?      &?     &?     &?      &?     &1,8,24    &  400 x  800\\
UMAX Supervista S12     &UMAX     &UMAX S-12       &all    &ok    &--    &ok     &ok    &1,8,24    & 1200 x 1200\\
UMAX Supervista S12     &UMAX     &UMAX S-12G      &all    &ok    &--    &ok     &ok    &1,8,24    & 1200 x 1200\\
UMAX Supervista S12     &UMAX     &Supervista S-12 &all    &ok    &--    &ok     &ok    &1,8,24    & 1200 x 1200\\
UMAX Astra 600S         &UMAX     &Astra 600S      &all    &ok    &--    &ok     &ok    &1,8,24    &  300 x  600\\
UMAX Astra 610S         &UMAX     &Astra 610S      &all    &ok    &--    &ok     &ok    &1,8,24    &  300 x  600\\
UMAX Astra 1200S        &UMAX     &Astra 1200S     &all    &ok    &--    &ok     &ok    &1,8,24,30 &  600 x 1200\\
UMAX Astra 1220S        &UMAX     &Astra 1220S     &all    &ok    &--    &ok     &ok    &1,8,24,36 &  600 x 1200\\
UMAX UC630              &UMAX     &UC630           &$<$1.6 &ok    &?     &bad    &bad   &1,8,24    &  300 x  300\\
UMAX UC630              &UMAX     &UC630           &$\ge$1.6&ok   &?     &ok     &ok    &1,8,24    &  300 x  300\\
UMAX UG630              &UMAX     &UG630           &all    &ok    &?     &ok     &--    &1,8       &  300 x  300\\
UMAX UG80               &UMAX     &UG80            &all    &ok    &?     &ok     &--    &1,8       &  300 x  300\\
UMAX UC840              &UMAX     &UC840           &$<$1.6 &ok    &?     &bad    &bad   &1,8,24    &  400 x  400\\
UMAX UC840              &UMAX     &UC840           &$\ge$1.6&ok   &?     &ok     &ok    &1,8,24    &  400 x  400\\
UMAX UC1200S            &UMAX     &UC1200S         &?      &bad   &bad   &bad    &bad   &1,8,24    &  600 x  600\\
UMAX UC1200SE           &UMAX     &UC1200S         &?      &?     &?     &?      &?     &1,8,24    &  600 x 1200\\
UMAX UC1260             &UMAX     &UC1260          &?      &?     &?     &?      &?     &1,8,24    &  600 x  600\\
UMAX Mirage IIse        &UMAX     &Mirage IIse     &?      &?     &?     &?      &?     &1,8,24    &  700 x 1400\\
UMAX Vista T630         &UMAX     &Vista-T630      &all(?) &ok    &--    &ok     &ok    &1,8,24    &  300 x  600\\
UMAX PageScan           &UMAX     &PSD             &all    &ok    &--    &ok     &ok    &1,8,24    &  300 x  300\\
UMAX PowerLookII        &UMAX     &PL-II           &some   &ok    &--    &ok     &ok    &1,8,24    &  600 x 1200\\
\hline
Linotype Hell Jade      &LinoHell &Office          &all    &ok    &--    &ok     &ok    &1,8,24    & 1200 x 1200\\
Linotype Hell Jade2     &LinoHell &Office 2        &all    &ok    &--    &ok     &ok    &1,8,24,30 &  600 x 1200\\
\hline
Highscreen:&&&&&&&&&\\
HS 5c                   &LinoHell &Office          &all    &ok    &--    &ok     &ok    &1,8,24,30 & 1200 x 1200\\
Scanboostar Premium     &LinoHell &Office 2        &all    &ok    &--    &ok     &ok    &1,8,24,30 &  600 x 1200\\
\hline
Escom:&&&&&&&&&\\
Image Scanner 256       &UMAX     &UG80            &all    &ok    &?     &ok     &--    &1,8       &  300 x  300\\
\hline
\end{tabular}
\normalsize


If you own a UMAX-scanner other than the ones listed above, it may work with SANE.
It depends on the informations the scanner returns to the umax-backend. If the
data-block is large enough, the backend prints a warning and continues, but it is
possible that not everything works fine. I suggest you hold one hand on the
power-button of the scanner while you try the first scans!




\newpage
\section{General Informations}


\subsection{BUGS:}

\begin{itemize}
\item resolutions greater than 600 dpi sometimes don't work! 
\item calibration by driver does not work for Astra 6X0S and Vista S6E
\end{itemize}


\subsection{Authors:}

Oliver Rauch, parts of the low-level-driver by Michael K. Johnson


\subsection{See also:}

sane, introduction, backends, frontends, sane-scsi, sane-dll



\end{document}
