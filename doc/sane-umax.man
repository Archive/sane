.TH sane-umax 5 "17th october 1998"
.IX sane-umax

.SH NAME

sane-umax - SANE backend for UMAX scanners

.SH DESCRIPTION

The
.B sane-umax
library implements a SANE backend that provides acces to several
UMAX-SCSI-scanners and some compatible scanners,
Parallel-port-scanners are not supported!
At present the following scanners are known
to work at least a bit with this backend:

 UMAX Vista S-6
 UMAX Vista S-6E
 UMAX Vista S-8 - not tested
 UMAX Supervista S-12
 UMAX Supervista S-12G
 UMAX Astra 600S
 UMAX Astra 610S
 UMAX Astra 1200S
 UMAX Astra 1220S
 UMAX UG80
 UMAX UC630 - firmware dependend
 UMAX UG630
 UMAX UC840 - firmware dependend
 UMAX UC1200S - does not work
 UMAX UC1200SE - not tested
 UMAX Mirage IIse - not tested
 UMAX Vista T630
 UMAX PageScan
 UMAX PowerLookII
 LinotypeHell Jade (ID="Office")
 LinotypeHell Jade2 (ID="Office 2")
 Highscreen Scanboostar Premium (ID="LinoHell","Office 2")
 Highscreen HS 5c (ID="LinoHell","Office")
 Escom Image Scanner 256 (ID="UMAX","UG80")

If you own a UMAX-scanner other than the ones listed above, it may work with SANE.
It depends on the informations the scanner returns to the umax-backend. If the
data-block is large enough, the backend prints a warning and continues, but it is
possible that not everything works fine.
.B I suggest you hold one hand on the power-button of the scanner while you try the first scans!

.SH DOCUMENTATION

For detailled informations see:
sane-umax-doc.html and sane-umax-doc.dvi

.SH CONFIGURATION

The configuration file for this backend resides in 
	/usr/local/etc/sane.d/umax.conf. 

Its contents is a list of device names that correspond to UMAX scanners. Empty lines
and lines starting with a hash mark (#) are ignored. A sample configuration file is
shown below: 

 #scsi Vendor Model Type Bus Channel ID LUN 
 scsi UMAX * Scanner * * * * * 
 /dev/scanner 
 # this is a comment 
 /dev/sge 

The special device name must be a generic SCSI device or a symlink to such a device.
To find out to which device your scanner is assigned and how you have to set the
permissions of that device, have a look at sane-scsi. 

.SH SCSI ADAPTER TIPS

The SCSI-adapters that are shipped with some Umax-scanners are not supported by Linux
and most other platforms, so you typically need to purchase another SCSI-adapter that
is supported by your platform. See the relevant hardware FAQs and HOWTOs for your
platform for more information. 

The UMAX-scanners do block the scsi-bus for a few seconds while scanning. It is not
necessary to connect the scanner to its own SCSI-adapter. But if you need short
response time for your SCSI-harddisk (e.g. if your computer is a file-server), I
suggest you use an own SCSI-adapter for your UMAX-scanner. 

See also: sane-scsi(5)

.SH FILES

The backend configuration file:
 /usr/local/etc/sane.d/umax.conf

The static library implementing this backend:
 /usr/local/lib/sane/libsane-umax.a

The shared library implementing this backend :
 /usr/local/lib/sane/libsane-umax.so
 (present on systems that support dynamic loading)

.SH ENVIRONMENT

.B SANE_DEBUG_UMAX
 If the library was compiled with debug support enabled, this environment
variable controls the debug level for this backend. E.g., a value of 128
requests all debug output to be printed. Smaller levels reduce verbosity:
SANE_DEBUG_UMAX values

.DS
.sp 
.ft CR
.nf
 Number  Remark
 0       print important errors (printed each time)
 1       print errors
 2       print sense
 3       print warnings
 4       print scanner-inquiry
 5       print informations
 6       print less important informations
 7       print called procedures
 8       print reader_process messages
 10      print called sane-init-routines
 11      print called sane-procedures
 12      print sane infos
 13      print sane option-control messages
Example:
export SANE_DEBUG_UMAX=8

.SH BUGS

Calibration by driver does not work with Astra 6X0S and Vista S6E

X-resolutions greater than 600 dpi sometimes make problems

.SH AUTHORS

Oliver Rauch, parts of the low-level-driver by Michael K. Johnson

.SH EMAIL-CONTACT
Oliver.Rauch@Wolfsburg.DE
