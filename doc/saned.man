.TH saned 1 "21 Nov 1998"
.IX saned
.SH NAME
saned - SANE network daemon
.SH SYNOPSIS
.B saned
.RB [ -d
.RI [ n ]]
.SH DESCRIPTION
.B saned
is the SANE (Scanner Access Now Easy) daemon that allows remote clients
to access image acquisition devices available on the local host.
.SH OPTIONS
.PP
The
.B -d
flag request that
.B saned
run in debug mode (as opposed to
.BR inetd (8)
mode).  In this mode,
.B saned
explicitly waits for a connection request.  When compiled with
debugging enabled, this flag may be followed by a number to request
debug info.  The larger the number, the more verbose the debug output.
E.g., -d128 will request printing of all debug info.
.SH CONFIGURATION
First and foremost: please do
.I not
install
.B saned
as setuid root without due consideration.  Especially when using
dynamic linking, there is a potential for introducing security holes
when running this program as root.
.PP
The contents of the
.I saned.conf
file is a list of host names that are permitted to use local
SANE devices.  Empty lines and lines starting with a hash mark (#) are
ignored.  A sample configuration file is shown below:
.PP
.RS
scan-client.somedomain.firm
.br
# this is a comment
.br
localhost
.RE
.PP
The case of the host names does not matter, so AHost.COM is considered
identical to ahost.com.

For
.B saned
to work properly, it is also necessary to add a configuration line to
.IR /etc/inetd.conf .
The configuration line normally looks like this:
.PP
.RS
sane stream tcp nowait saned.saned @SBINDIR@/saned saned
.RE
.PP
However, if your system uses
.BR tcpd (8)
for additional security screening, you may want to use a line of the
following form instead:
.PP
.RS
sane stream tcp nowait saned.saned /usr/sbin/tcpd saned
.RE
.PP
Note that both examples assume that there is a
.B saned
group and a
.B saned
user.  If you follow this example, please make sure that the 
access permissions on the special device are set such that
.B saned
can access the scanner (the program generally needs read and
write access to scanner devices).
.PP
Finally, it is also necessary to add a line of the following form to
.IR /etc/services :
.PP
.RS
sane 6566/tcp # SANE network scanner daemon
.RE
.PP
Note that port number 6566 has not been officially assigned to the
SANE network protocol and may thus change in the future.
.PP
.SH FILES
.TP
.I /etc/hosts.equiv
The hosts listed in this file are permitted to access all local SANE
devices.  Caveat: this file imposes serious security risks and its use
is not recommended.
.TP
.I @CONFIGDIR@/saned.conf
Contains a list of hosts permitted to access local SANE devices (see
also description of
.B SANE_CONFIG_DIR
below).
.SH ENVIRONMENT
.TP
.B SANE_CONFIG_DIR
This environment variable specifies the list of directories that may
contain the configuration file.  Under UNIX, the directories are
separated by a colon (`:'), under OS/2, they are separated by a
semi-colon (`;').  If this variable is not set, the configuration file
is searched in two default directories: first, the current working
directory (".") and then in @CONFIGDIR@.  If the value of the
environment variable ends with the directory separator character, then
the default directories are searched after the explicitly specified
directories.  For example, setting
.B SANE_CONFIG_DIR
to "/tmp/config:" would result in directories "tmp/config", ".", and
"@CONFIGDIR@" being searched (in this order).

.SH "SEE ALSO"
scanimage(1), xscanimage(1), xcam(1), sane\-dll(5), sane\-hp(5),
sane\-mustek(5), sane\-net(5), sane\-pnm(5), sane\-pint(5), sane\-qcam(5),
sane\-umax(5)
.SH AUTHOR
David Mosberger
