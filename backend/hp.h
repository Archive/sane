/* sane - Scanner Access Now Easy.
   Copyright (C) 1997 Geoffrey T. Dairiki
   This file is part of the SANE package.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

   As a special exception, the authors of SANE give permission for
   additional uses of the libraries contained in this release of SANE.

   The exception is that, if you link a SANE library with other files
   to produce an executable, this does not by itself cause the
   resulting executable to be covered by the GNU General Public
   License.  Your use of that executable is in no way restricted on
   account of linking the SANE library code into it.

   This exception does not, however, invalidate any other reasons why
   the executable file might be covered by the GNU General Public
   License.

   If you submit changes to SANE to the maintainers to be included in
   a subsequent release, you agree by submitting the changes that
   those changes may be distributed with this exception intact.

   If you write modifications of your own for SANE, it is your choice
   whether to permit this exception to apply to your modifications.
   If you do not wish that, delete this exception notice.

   This file is part of a SANE backend for HP Scanners supporting
   HP Scanner Control Language (SCL).
*/

#ifndef HP_H_INCLUDED
#define HP_H_INCLUDED
#include <limits.h>
#include <sys/types.h>
#include <sane/sane.h>

#undef BACKEND_NAME
#define BACKEND_NAME	hp
#include <sane/sanei_debug.h>

/* FIXME: these should be options? */
#undef ENABLE_7x12_TONEMAPS
#define ENABLE_16x16_DITHERS
#define ENABLE_10BIT_MATRIXES

#define FAKE_COLORSEP_MATRIXES

#undef ENABLE_CUSTOM_MATRIX

#define HP_CONFIG_FILE 	 STRINGIFY(BACKEND_NAME) ".conf"

#define MM_PER_INCH		25.4
#define DEVPIX_PER_INCH		300.0
#define MM_PER_DEVPIX		(MM_PER_INCH / DEVPIX_PER_INCH)

/*
 * Macros for testing return values of functions which
 * return SANE_Status types.
 */
#define FAILED(status)		(status != SANE_STATUS_GOOD)
#define UNSUPPORTED(status)	(status == SANE_STATUS_UNSUPPORTED)
#define RETURN_IF_FAIL(try)	do {					\
  SANE_Status status = (try);						\
  if (FAILED(status))							\
      return status;							\
} while (0)
#define CATCH_RET_FAIL(try, catch) do {					\
  SANE_Status status = (try);						\
  if (FAILED(status)) { (catch); return status; }			\
} while (0)

#ifndef DBG_LEVEL
#define DBG_LEVEL	PASTE(sanei_debug_, BACKEND_NAME)
#endif
#ifndef NDEBUG
# define DBGDUMP(level, buf, size) \
    do { if (DBG_LEVEL >= (level)) sanei_hp_dbgdump(buf, size); } while (0)
#else
# define DBGDUMP(level, buf, size)
#endif


 /*
  *
  */

typedef int 		hp_bool_t;
typedef unsigned char	hp_byte_t;

/* option.c */
typedef const SANE_Option_Descriptor *		HpSaneOption;

typedef const struct hp_choice_s *		HpChoice;
typedef const struct hp_option_s *		HpOption;
typedef const struct hp_option_descriptor_s *	HpOptionDescriptor;
typedef struct hp_optset_s *			HpOptSet;

/* accessor.c */
typedef struct hp_data_s *		HpData;
typedef const struct hp_accessor_s *	HpAccessor;
typedef const struct hp_accessor_vector_s *	HpAccessorVector;
typedef const struct hp_accessor_choice_s *	HpAccessorChoice;

/* device.c */
typedef struct hp_device_s *	HpDevice;

/* handle.c */
typedef struct hp_handle_s *	HpHandle;

/* scl .c */
#if INT_MAX > 30000
typedef	int HpScl;
#else
typedef long int HpScl;
#endif

/* scsi.c */
typedef struct hp_scsi_s * 	HpScsi;

/* hp.c */
#ifndef NDEBUG
void sanei_hp_dbgdump (const void * bufp, size_t len);
#endif

/* hpmem.c */
void *	sanei_hp_alloc(size_t sz);
void *	sanei_hp_allocz(size_t sz);
void *	sanei_hp_memdup(const void * src, size_t sz);
char *	sanei_hp_strdup(const char * str);
void *	sanei_hp_realloc(void * ptr, size_t sz);
void	sanei_hp_free(void * ptr);
void	sanei_hp_free_all(void);

#endif /* HP_H_INCLUDED */
