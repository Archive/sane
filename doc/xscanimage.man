.TH xscanimage 1 "13 May 1998"
.IX xscanimage
.SH NAME
xscanimage - scan an image
.SH SYNOPSIS
.B xscanimage
.RB [ --display
.IR d ]
.RB [ --no-xshm ]
.RB [ --[no-]show-events ]
.RB [ --sync ]
.RB [ --debug-level
.IR n ]
.RI [ devicename ]
.SH DESCRIPTION
.B xscanimage
provides a graphical user-interface to control an image
acquisition device such as a flatbed scanner or a camera.  It allows
previewing and scanning invidual images and can be invoked either
directly from the command-line or through The GIMP image manipulation
program.  In the former case,
.B xscanimage
acts as a stand-alone program that saves acquired images in a suitable
PNM format (PBM for black-and-white images, PGM for grayscale images,
and PPM for color images).  In the latter case, the images are
directly passed to The GIMP for further processing.

.B xscanimage
accesses image acquisition devices through the SANE (Scanner Access
Now Easy) interface.  The list of available devices depends on
installed hardware and configuration.  When invoked without an
explicit devicename argument,
.B xscanimage
presents a dialog listing all known and available devices.  To access
an available device that is not known to the system, the devicename
must be specified explicitly.
.SH RUNNING UNDER THE GIMP
To run
.B xscanimage
under the
.BR gimp (1),
simply copy it to one of the
.BR gimp (1)
plug-ins directories.  If you want to conserve disk-space, you can
create a symlink instead.  For example, the command
.PP
.RS
ln -s @BINDIR@/xscanimage ~/.gimp/plug-ins/
.RE
.PP
adds a symlink for the
.B xscanimage
binary to the user's plug-ins directory.  After creating this symlink,
.B xscanimage
will be queried by
.BR gimp (1)
the next time it's invoked.  From then on,
.B xscanimage
can be invoked through "Xtns->Acquire Image->Device dialog..." menu entry.

You'll also find that the "Xtns->Acquire Image" menu contains short-cuts
to the SANE devices that were available at the time the
.B xscanimage
was queried.  For example, the first PNM pseudo-device is typically
available as the short-cut "Xtns->Acquire Image->pnm:0".
Note that
.BR gimp (1)
caches these short-cuts in ~/.gimp/pluginrc.  Thus, when the list of
available devices changes (e.g., a new scanner is installed), then it
is typically desirable to rebuild this cache.  To do this, you can
either
.BR touch (1)
the
.B xscanimage
binary (e.g., "touch @BINDIR@/xscanimage") or delete the plug-ins cache
(e.g., "rm ~/.gimp/plug-ins").  Either way, invoking
.BR gimp (1)
afterwards will cause the pluginrc to be rebuilt.
.SH OPTIONS
.PP
The
.B --display
flag selects the X11 display used to present the graphical user-interface
(see
.BR X (1)
for details).
.PP
The
.B --no-xshm
flag requests not to use shared memory images.  Shared memory images
usually enhance performance but cause problems with some buggy X11
servers.  Unless your X11 server dies when running this program, there
is no need or advantage to specify this flag.
.PP
The
.B --show-events
flag requests the display of X11 events.  This is for
debugging only.  Flag
.B --no-show-events
turns off the display of X11 events.
.PP
The
.B --sync
flag requests a synchronous connection with the X11 server.  This is for
debugging purposes only.
.SH FILES
.TP
.I $HOME/.sane/xscanimage/xscanimage.rc
This files holds the user preferences.  Normally, this file should not
be manipulated directly.  Instead, the user should customize the
program through the "Preferences" dialog.
.TP
.I $HOME/.sane/xscanimage/devicename.rc
For each device, there is one rc-file that holds the saved settings
for that particular device.  Normally, this file should not be
manipulated directly.  Instead, the user should use the
.B xscanimage
interface to select appropriate values and then save the device
settings using the "Preferences->Save Device Settings" menubar entry.
.TP
.I $HOME/.sane/preview-devicename.ppm
After acquiring a preview,
.B xscanimage
normally saves the preview image in this device-specific file.  Thus,
next time the program is started up, the program can present the old
preview image.  This feature can be turned off through the
"Preferences->Preview Options..." dialog.
.TP
.I @DATADIR@/sane-style.rc
This system-wide file controls the aspects of the user-interface such
as colors and fonts.  It is a GTK style file and provides fine control
over the visual aspects of the user-interface.
.TP
.I $HOME/.sane/sane-style.rc
This file serves the same purpose as the system-wide style file.  If
present, it takes precedence over the system wide style file.
.SH "SEE ALSO"
gimp(1), scanimage(1), sane\-dll(5), sane\-dmc(5), sane\-epson(5),
sane\-hp(5), sane\-microtek(5), sane\-mustek(5), sane\-net(5), sane\-pnm(5),
sane\-pint(5), sane\-qcam(5), sane\-umax(5)
.SH AUTHOR
Tristan Tarrant, Andreas Beck, and David Mosberger
