dnl Process this file with autoconf to produce a configure script. -*-sh-*-
AC_INIT(include/sane/sane.h)
AC_CONFIG_HEADER(include/sane/config.h)
AC_PREREQ(2.10)dnl		dnl Minimum Autoconf version required.
AC_ARG_PROGRAM

# version code:
V_MAJOR=1
V_MINOR=0

PACKAGE=sane
VERSION=${V_MAJOR}.${V_MINOR}
PACKAGE_VERSION="$PACKAGE-$VERSION"
AC_DEFINE_UNQUOTED(PACKAGE, "$PACKAGE")
AC_DEFINE_UNQUOTED(VERSION, "$VERSION")
AC_DEFINE_UNQUOTED(PACKAGE_VERSION, "$PACKAGE_VERSION")
AC_SUBST(PACKAGE)
AC_SUBST(VERSION)
AC_SUBST(PACKAGE_VERSION)

dnl Checks for programs.
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_PROG_CPP
AC_PROG_GCC_TRADITIONAL
AC_AIX
AC_MINIX
AC_ISC_POSIX

dnl Checks for GNOME
gnome=yes
AC_ARG_ENABLE(gnome,
[  --disable-gnome		disable use of gnome],
	gnome-$enableval, gnome=yes)

if test "$gnome" = yes; then
  GNOME_INIT
  AC_DEFINE(USE_GNOME)
fi
INCLUDES="${INCLUDES} -I/usr/local/include"
CPPFLAGS="${CPPFLAGS} -D_GNU_SOURCE"
if test "${ac_cv_prog_gcc}" = "yes"; then
  CFLAGS="${CFLAGS} -Wall"
fi

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(fcntl.h unistd.h libintl.h libc.h sys/dsreq.h sys/select.h \
    sys/time.h \
    sys/scanio.h scsi.h sys/scsi.h sys/scsicmd.h sys/scsiio.h \
    bsd/dev/scsireg.h scsi/sg.h /usr/src/linux/include/scsi/sg.h io/cam/cam.h \
    camlib.h os2.h sys/socket.h sys/io.h asm/io.h gscdds.h sys/hw.h \
    sys/types.h sys/scsi/sgdefs.h sys/scsi/targets/scgio.h apollo/scsi.h)

dnl Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_C_INLINE
AC_TYPE_SIGNAL
AC_TYPE_SIZE_T
AC_TYPE_PID_T
AC_CHECK_TYPE(ssize_t, long)
AC_CHECK_TYPE(u_char, unsigned char)
AC_CHECK_TYPE(u_int, unsigned int)
AC_CHECK_TYPE(u_long, unsigned long)

if test "$ac_cv_header_sys_scsiio_h" = "yes" \
     -a "$ac_cv_header_scsi_h" = "yes";
then
 AC_TRY_COMPILE([
#include <sys/scsiio.h>
#include <scsi.h>
],[scsireq_t req],,
   AC_DEFINE(scsireq_t, struct scsireq_t))
fi

dnl Checks for libraries.

AC_CHECK_LIB(scsi, scsireq_enter)	# FreeBSD needs this
AC_CHECK_LIB(m,sqrt)
AC_CHECK_LIB(intl,gettext)
AC_CHECK_LIB(nsl,gethostbyaddr)
AC_CHECK_LIB(socket,socket)
AC_CHECK_LIB(syslog,syslog)             # OS/2 needs this
AC_CHECK_LIB(jpeg,jpeg_start_decompress)

dnl Checks for library functions.
AM_FUNC_ALLOCA
AC_FUNC_MMAP
AC_CHECK_FUNCS(atexit ioperm mkdir scsireq_enter sigprocmask \
    strdup strndup strftime strstr strsep strtod snprintf usleep \
    strcasecmp strncasecmp _portaccess)

dnl initialize libtool
AM_PROG_LIBTOOL

dnl Checks for libraries.

#### Choose a window system.

AC_PATH_X
if test "$no_x" = yes; then
  window_system=none
else
  window_system=x11
fi

if test "${enable_dynamic}" != "no"; then
  AC_CHECK_HEADERS(dlfcn.h,
  [AC_CHECK_LIB(dl,dlopen)
   AC_CHECK_FUNCS(dlopen, enable_dynamic=yes,)],)

  # HP/UX DLL handling
  AC_CHECK_HEADERS(dl.h,
  [AC_CHECK_LIB(dld,shl_load)
   AC_CHECK_FUNCS(shl_load, enable_dynamic=yes,)],)
fi

AM_PATH_GTK(0.99.13, HAVE_GTK=yes, )

# Change CFLAGS temporarily so that C_SWITCH_X_SITE gets used
# for the tests that follow.  We set it back to REAL_CFLAGS later on.

if test "${HAVE_GTK}" = "yes"; then
  XSCAN="xscanimage xcam"

  # According to Owen Taylor, GTK_CFLAGS is _guaranteed_ to contain
  # -D and -I flags only, i.e., it really is GTK_CPPFLAGS...
  saved_CPPFLAGS="${CPPFLAGS}"
  saved_LIBS="${LIBS}"
  CPPFLAGS="${CPPFLAGS} ${GTK_CFLAGS}"
  LIBS="${LIBS} ${GTK_LIBS}"

  AC_CHECK_FUNCS(gtk_tooltips_set_tips gtk_events_pending)
  if test "${ac_cv_func_gtk_events_pending}" = "no"; then
    saved_CPPFLAGS="${saved_CPPFLAGS} -Dgtk_events_pending=gdk_events_pending"
  fi

  AC_CHECK_HEADERS(libgimp/gimp.h,GIMP_LIBS="-lgimp")

  CPPFLAGS="${saved_CPPFLAGS}"
  LIBS="${saved_LIBS}"
fi
AC_SUBST(INCLUDES)
AC_SUBST(XSCAN)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)
AC_SUBST(GIMP_LIBS)
CPPFLAGS="${CPPFLAGS} -DPATH_SANE_CONFIG_DIR=\$(configdir) \
	  -DPATH_SANE_DATA_DIR=\$(datadir) \
	  -DV_MAJOR=${V_MAJOR} -DV_MINOR=${V_MINOR}"

if test "${ac_cv_header_jconfig_h}" = "no"; then
  echo "disabling DC210 backend (failed to find JPEG library)"
  DC210=
else
  echo "enabling DC210 backend"
  DC210=dc210
fi
AC_SUBST(DC210)
if test "${ac_cv_header_sys_scanio_h}" = "no"; then
  echo "disabling PINT backend"
  PINT=
else
  echo "enabling PINT backend"
  PINT=pint
fi
AC_SUBST(PINT)
if test "${ac_cv_func_ioperm}" = "no" \
    && test "${ac_cv_func__portaccess}" = "no"
then
  echo "disabling QuickCam backend"
  QCAM=
else
  echo "enabling QuickCam backend"
  QCAM=qcam
fi
AC_SUBST(QCAM)
if test "${ac_cv_header_sys_socket_h}" = "no"; then
  echo "disabling NET backend"
  NET=
  SANED=
else
  echo "enabling NET backend"
  NET=net
  SANED=saned
fi
AC_SUBST(NET)
AC_SUBST(SANED)
if test "${enable_shared}" = "no"; then
  enable_preload=yes
fi
if test "${enable_dynamic}" != yes || test "${enable_preload}" = "yes"; then
  echo "preloading backends into DLL"
  DLL_PRELOAD="\$(PRELOADABLE_BACKENDS)"
else
  DLL_PRELOAD=""
fi
AC_SUBST(V_MAJOR)
AC_SUBST(V_MINOR)
AC_SUBST(DLL_PRELOAD)

AC_OUTPUT([Makefile lib/Makefile sanei/Makefile frontend/Makefile
	   japi/Makefile backend/Makefile include/Makefile doc/Makefile
	   tools/Makefile],)

echo "****************************************************************"
echo "* Please be sure to read file PROBLEMS in this directory       *"
echo "* BEFORE running any of the SANE applications.  Some devices   *"
echo "* may be damaged by inproper operation, so please do heed this *"
echo "* advice.                                                      *"
echo "****************************************************************"
