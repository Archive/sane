/* sane - Scanner Access Now Easy.
   Copyright (C) 1997 BYTEC GmbH Germany
   Written by Helmut Koeberle, Email: helmut.koeberle@bytec.de

   This file is part of the SANE package.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

   As a special exception, the authors of SANE give permission for
   additional uses of the libraries contained in this release of SANE.

   The exception is that, if you link a SANE library with other files
   to produce an executable, this does not by itself cause the
   resulting executable to be covered by the GNU General Public
   License.  Your use of that executable is in no way restricted on
   account of linking the SANE library code into it.

   This exception does not, however, invalidate any other reasons why
   the executable file might be covered by the GNU General Public
   License.

   If you submit changes to SANE to the maintainers to be included in
   a subsequent release, you agree by submitting the changes that
   those changes may be distributed with this exception intact.

   If you write modifications of your own for SANE, it is your choice
   whether to permit this exception to apply to your modifications.
   If you do not wish that, delete this exception notice. */

/* This file implements the sane-api */

#include <sane/config.h>

#include <limits.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <sane/sane.h>
#include <sane/saneopts.h>
#include <sane/sanei_scsi.h>
#include <canon.h>

#define BACKEND_NAME canon
#include <sane/sanei_backend.h>

#ifndef PATH_MAX
#define PATH_MAX	1024
#endif

#include <sane/sanei_config.h>
#define CANON_CONFIG_FILE "canon.conf"

static int num_devices = 0;
static CANON_Device *first_dev = NULL;
static CANON_Scanner *first_handle = NULL;

static const SANE_String_Const mode_list[] =
{
  "Lineart", "Halftone", "Gray", "Color",
  0
};

static const SANE_String_Const papersize_list[] =
{
  "A4", "Letter", "B5", "Maximal",
  0
};

#include "canon-scsi.c"

static size_t
max_string_size (const SANE_String_Const strings[])
{
  size_t size, max_size = 0;
  int i;
  DBG (11, ">> max_string_size\n");

  for (i = 0; strings[i]; ++i)
    {
      size = strlen (strings[i]) + 1;
      if (size > max_size)
	max_size = size;
    }

  DBG (11, "<< max_string_size\n");
  return max_size;
}

static SANE_Status
attach (const char *devnam, CANON_Device ** devp)
{
  SANE_Status status;
  CANON_Device *dev;

  int fd;
  unsigned char ibuf[36], ebuf[74], mbuf[12];
  size_t buf_size;
  char *str;
  DBG (11, ">> attach\n");

  for (dev = first_dev; dev; dev = dev->next)
    {
      if (strcmp (dev->sane.name, devnam) == 0)
	{
	  if (devp)
	    *devp = dev;
	  return (SANE_STATUS_GOOD);
	}
    }

  DBG (3, "attach: opening %s\n", devnam);
  status = sanei_scsi_open (devnam, &fd, NULL, NULL);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "attach: open failed: %s\n", sane_strstatus (status));
      return (status);
    }

  DBG (3, "attach: sending (standard) INQUIRY\n");
  memset (ibuf, 0, sizeof (ibuf));
  buf_size = sizeof (ibuf);
  status = inquiry (fd, 0, ibuf, &buf_size);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "attach: inquiry failed: %s\n", sane_strstatus (status));
      sanei_scsi_close (fd);
      return (status);
    }

  if (ibuf[0] != 6
      || strncmp (ibuf + 8, "CANON", 5) != 0
      || strncmp (ibuf + 16, "IX-", 3) != 0)
    {
      DBG (1, "attach: device doesn't look like a Canon scanner\n");
      sanei_scsi_close (fd);
      return (SANE_STATUS_INVAL);
    }

  DBG (3, "attach: sending TEST_UNIT_READY\n");
  status = test_unit_ready (fd);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "attach: test unit ready failed (%s)\n",
	   sane_strstatus (status));
      sanei_scsi_close (fd);
      return (status);
    }

  DBG (3, "attach: sending MEDIUM POSITION\n");
  status = medium_position (fd);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "attach: MEDIUM POSTITION failed\n");
      sanei_scsi_close (fd);
      return (SANE_STATUS_INVAL);
    }

  DBG (3, "attach: sending (extended) INQUIRY\n");
  memset (ebuf, 0, sizeof (ebuf));
  buf_size = sizeof (ebuf);
  status = inquiry (fd, 1, ebuf, &buf_size);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "attach: (extended) INQUIRY failed\n");
      sanei_scsi_close (fd);
      return (SANE_STATUS_INVAL);
    }

  DBG (3, "attach: sending MODE SENSE\n");
  memset (mbuf, 0, sizeof (mbuf));
  buf_size = sizeof (mbuf);
  status = mode_sense (fd, mbuf, &buf_size);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "attach: MODE_SENSE failed\n");
      sanei_scsi_close (fd);
      return (SANE_STATUS_INVAL);
    }

  sanei_scsi_close (fd);

  dev = malloc (sizeof (*dev));
  if (!dev)
    return (SANE_STATUS_NO_MEM);
  memset (dev, 0, sizeof (*dev));

  dev->sane.name = strdup (devnam);
  dev->sane.vendor = "CANON";
  str = malloc (16 + 1);
  memset (str, 0, sizeof (str));
  strncpy (str, ibuf + 8, 16);
  dev->sane.model = str;
  dev->sane.type = "flatbed scanner";

  DBG (5, "dev->sane.name = %s\n", dev->sane.name);
  DBG (5, "dev->sane.vendor = %s\n", dev->sane.vendor);
  DBG (5, "dev->sane.model = %s\n", dev->sane.model);
  DBG (5, "dev->sane.type = %s\n", dev->sane.type);

  dev->info.xres_default = (ebuf[5] * 256) + ebuf[6];
  DBG (5, "xres_default=%d\n", dev->info.xres_default);
  dev->info.xres_range.max = (ebuf[10] * 256) + ebuf[11];
  DBG (5, "xres_range.max=%d\n", dev->info.xres_range.max);
  dev->info.xres_range.min = (ebuf[14] * 256) + ebuf[15];
  DBG (5, "xres_range.min=%d\n", dev->info.xres_range.min);
  dev->info.xres_range.quant = ebuf[9] >> 4;
  DBG (5, "xres_range.quant=%d\n", dev->info.xres_range.quant);

  dev->info.yres_default = (ebuf[7] * 256) + ebuf[8];
  DBG (5, "yres_default=%d\n", dev->info.yres_default);
  dev->info.yres_range.max = (ebuf[12] * 256) + ebuf[13];
  DBG (5, "yres_range.max=%d\n", dev->info.yres_range.max);
  dev->info.yres_range.min = (ebuf[16] * 256) + ebuf[17];
  DBG (5, "yres_range.min=%d\n", dev->info.yres_range.min);
  dev->info.yres_range.quant = ebuf[9] & 0x0f;
  DBG (5, "xres_range.quant=%d\n", dev->info.xres_range.quant);

  dev->info.x_range.min = 0;
  dev->info.x_range.max = (ebuf[20] * 256 * 256 * 256)
      + (ebuf[21] * 256 * 256) + (ebuf[22] * 256) + ebuf[23] - 1;
  DBG (5, "x_range.max=%d\n", dev->info.x_range.max);
  dev->info.x_range.quant = 0;

  dev->info.y_range.min = 0;
  dev->info.y_range.max = (ebuf[24] * 256 * 256 * 256)
      + (ebuf[25] * 256 * 256) + (ebuf[26] * 256) + ebuf[27] - 1;
  DBG (5, "y_range.max=%d\n", dev->info.y_range.max);
  dev->info.y_range.quant = 0;

  dev->info.x_adf_range.max = (ebuf[30] * 256 * 256 * 256)
      + (ebuf[31] * 256 * 256) + (ebuf[32] * 256) + ebuf[33] - 1;
  DBG (5, "x_adf_range.max=%d\n", dev->info.x_adf_range.max);
  dev->info.y_adf_range.max = (ebuf[34] * 256 * 256 * 256)
      + (ebuf[35] * 256 * 256) + (ebuf[36] * 256) + ebuf[37] - 1;
  DBG (5, "y_adf_range.max=%d\n", dev->info.y_adf_range.max);

  dev->info.bmu = mbuf[6];
  DBG (5, "bmu=%d\n", dev->info.bmu);
  dev->info.mud = (mbuf[8] * 256) + mbuf[9];
  DBG (5, "mud=%d\n", dev->info.mud);

  dev->info.brightness_range.min = 1;
  dev->info.brightness_range.max = 255;
  dev->info.brightness_range.quant = 0;

  dev->info.contrast_range.min = 1;
  dev->info.contrast_range.max = 255;
  dev->info.contrast_range.quant = 0;

  dev->info.threshold_range.min = 1;
  dev->info.threshold_range.max = 255;
  dev->info.threshold_range.quant = 0;

  ++num_devices;
  dev->next = first_dev;
  first_dev = dev;

  if (devp)
    *devp = dev;

  DBG (11, "<< attach\n");
  return (SANE_STATUS_GOOD);
}

static SANE_Status
init_options (CANON_Scanner * s)
{
  int i;
  DBG (11, ">> init_options\n");

  memset (s->opt, 0, sizeof (s->opt));
  memset (s->val, 0, sizeof (s->val));

  for (i = 0; i < NUM_OPTIONS; ++i)
    {
      s->opt[i].size = sizeof (SANE_Word);
      s->opt[i].cap = SANE_CAP_SOFT_SELECT | SANE_CAP_SOFT_DETECT;
    }

  s->opt[OPT_NUM_OPTS].title = SANE_TITLE_NUM_OPTIONS;
  s->opt[OPT_NUM_OPTS].desc = SANE_DESC_NUM_OPTIONS;
  s->opt[OPT_NUM_OPTS].cap = SANE_CAP_SOFT_DETECT;
  s->val[OPT_NUM_OPTS].w = NUM_OPTIONS;

  /* "Mode" group: */
  s->opt[OPT_MODE_GROUP].title = "Scan Mode";
  s->opt[OPT_MODE_GROUP].desc = "";
  s->opt[OPT_MODE_GROUP].type = SANE_TYPE_GROUP;
  s->opt[OPT_MODE_GROUP].cap = 0;
  s->opt[OPT_MODE_GROUP].constraint_type = SANE_CONSTRAINT_NONE;

  /* scan mode */
  s->opt[OPT_MODE].name = SANE_NAME_SCAN_MODE;
  s->opt[OPT_MODE].title = SANE_TITLE_SCAN_MODE;
  s->opt[OPT_MODE].desc = SANE_DESC_SCAN_MODE;
  s->opt[OPT_MODE].type = SANE_TYPE_STRING;
  s->opt[OPT_MODE].size = max_string_size (mode_list);
  s->opt[OPT_MODE].constraint_type = SANE_CONSTRAINT_STRING_LIST;
  s->opt[OPT_MODE].constraint.string_list = mode_list;
  s->val[OPT_MODE].s = strdup (mode_list[0]);

  /* x resolution */
  s->opt[OPT_X_RESOLUTION].name = "X" SANE_NAME_SCAN_RESOLUTION;
  s->opt[OPT_X_RESOLUTION].title = "X " SANE_TITLE_SCAN_RESOLUTION;
  s->opt[OPT_X_RESOLUTION].desc = SANE_DESC_SCAN_RESOLUTION;
  s->opt[OPT_X_RESOLUTION].type = SANE_TYPE_INT;
  s->opt[OPT_X_RESOLUTION].unit = SANE_UNIT_DPI;
  s->opt[OPT_X_RESOLUTION].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_X_RESOLUTION].constraint.range = &s->hw->info.xres_range;
  s->val[OPT_X_RESOLUTION].w = 600;

  /* y resolution */
  s->opt[OPT_Y_RESOLUTION].name = "Y" SANE_NAME_SCAN_RESOLUTION;
  s->opt[OPT_Y_RESOLUTION].title = "Y " SANE_TITLE_SCAN_RESOLUTION;
  s->opt[OPT_Y_RESOLUTION].desc = SANE_DESC_SCAN_RESOLUTION;
  s->opt[OPT_Y_RESOLUTION].type = SANE_TYPE_INT;
  s->opt[OPT_Y_RESOLUTION].unit = SANE_UNIT_DPI;
  s->opt[OPT_Y_RESOLUTION].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_Y_RESOLUTION].constraint.range = &s->hw->info.yres_range;
  s->val[OPT_Y_RESOLUTION].w = 600;

  /* "Geometry" group: */
  s->opt[OPT_GEOMETRY_GROUP].title = "Geometry";
  s->opt[OPT_GEOMETRY_GROUP].desc = "";
  s->opt[OPT_GEOMETRY_GROUP].type = SANE_TYPE_GROUP;
  s->opt[OPT_GEOMETRY_GROUP].cap = SANE_CAP_ADVANCED;
  s->opt[OPT_GEOMETRY_GROUP].constraint_type = SANE_CONSTRAINT_NONE;

  /* top-left x */
  s->opt[OPT_TL_X].name = SANE_NAME_SCAN_TL_X;
  s->opt[OPT_TL_X].title = SANE_TITLE_SCAN_TL_X;
  s->opt[OPT_TL_X].desc = SANE_DESC_SCAN_TL_X;
  s->opt[OPT_TL_X].type = SANE_TYPE_INT;
  s->opt[OPT_TL_X].unit = SANE_UNIT_PIXEL;
  s->opt[OPT_TL_X].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_TL_X].constraint.range = &s->hw->info.x_range;
  s->val[OPT_TL_X].w = 0;

  /* top-left y */
  s->opt[OPT_TL_Y].name = SANE_NAME_SCAN_TL_Y;
  s->opt[OPT_TL_Y].title = SANE_TITLE_SCAN_TL_Y;
  s->opt[OPT_TL_Y].desc = SANE_DESC_SCAN_TL_Y;
  s->opt[OPT_TL_Y].type = SANE_TYPE_INT;
  s->opt[OPT_TL_Y].unit = SANE_UNIT_PIXEL;
  s->opt[OPT_TL_Y].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_TL_Y].constraint.range = &s->hw->info.y_range;
  s->val[OPT_TL_Y].w = 0;

  /* bottom-right x */
  s->opt[OPT_BR_X].name = SANE_NAME_SCAN_BR_X;
  s->opt[OPT_BR_X].title = SANE_TITLE_SCAN_BR_X;
  s->opt[OPT_BR_X].desc = SANE_DESC_SCAN_BR_X;
  s->opt[OPT_BR_X].type = SANE_TYPE_INT;
  s->opt[OPT_BR_X].unit = SANE_UNIT_PIXEL;
  s->opt[OPT_BR_X].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_BR_X].constraint.range = &s->hw->info.x_range;
  s->val[OPT_BR_X].w = s->hw->info.x_range.max;

  /* bottom-right y */
  s->opt[OPT_BR_Y].name = SANE_NAME_SCAN_BR_Y;
  s->opt[OPT_BR_Y].title = SANE_TITLE_SCAN_BR_Y;
  s->opt[OPT_BR_Y].desc = SANE_DESC_SCAN_BR_Y;
  s->opt[OPT_BR_Y].type = SANE_TYPE_INT;
  s->opt[OPT_BR_Y].unit = SANE_UNIT_PIXEL;
  s->opt[OPT_BR_Y].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_BR_Y].constraint.range = &s->hw->info.y_range;
  s->val[OPT_BR_Y].w = s->hw->info.y_range.max;

  /* "Enhancement" group: */
  s->opt[OPT_ENHANCEMENT_GROUP].title = "Enhancement";
  s->opt[OPT_ENHANCEMENT_GROUP].desc = "";
  s->opt[OPT_ENHANCEMENT_GROUP].type = SANE_TYPE_GROUP;
  s->opt[OPT_ENHANCEMENT_GROUP].cap = 0;
  s->opt[OPT_ENHANCEMENT_GROUP].constraint_type = SANE_CONSTRAINT_NONE;

  /* brightness */
  s->opt[OPT_BRIGHTNESS].name = SANE_NAME_BRIGHTNESS;
  s->opt[OPT_BRIGHTNESS].title = SANE_TITLE_BRIGHTNESS;
  s->opt[OPT_BRIGHTNESS].desc = SANE_DESC_BRIGHTNESS;
  s->opt[OPT_BRIGHTNESS].type = SANE_TYPE_INT;
  s->opt[OPT_BRIGHTNESS].unit = SANE_UNIT_NONE;
  s->opt[OPT_BRIGHTNESS].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_BRIGHTNESS].constraint.range = &s->hw->info.brightness_range;
  s->val[OPT_BRIGHTNESS].w = 128;

  /* contrast */
  s->opt[OPT_CONTRAST].name = SANE_NAME_CONTRAST;
  s->opt[OPT_CONTRAST].title = SANE_TITLE_CONTRAST;
  s->opt[OPT_CONTRAST].desc = SANE_DESC_CONTRAST;
  s->opt[OPT_CONTRAST].type = SANE_TYPE_INT;
  s->opt[OPT_CONTRAST].unit = SANE_UNIT_NONE;
  s->opt[OPT_CONTRAST].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_CONTRAST].constraint.range = &s->hw->info.contrast_range;
  s->val[OPT_CONTRAST].w = 128;

  /* threshold */
  s->opt[OPT_THRESHOLD].name = SANE_NAME_THRESHOLD;
  s->opt[OPT_THRESHOLD].title = SANE_TITLE_THRESHOLD;
  s->opt[OPT_THRESHOLD].desc = SANE_DESC_THRESHOLD;
  s->opt[OPT_THRESHOLD].type = SANE_TYPE_INT;
  s->opt[OPT_THRESHOLD].unit = SANE_UNIT_NONE;
  s->opt[OPT_THRESHOLD].constraint_type = SANE_CONSTRAINT_RANGE;
  s->opt[OPT_THRESHOLD].constraint.range = &s->hw->info.threshold_range;
  s->val[OPT_THRESHOLD].w = 128;

  DBG (11, "<< init_options\n");
  return SANE_STATUS_GOOD;
}

static SANE_Status
do_cancel (CANON_Scanner * s)
{
  DBG (11, ">> do_cancel\n");

  s->scanning = SANE_FALSE;

  if (s->fd >= 0)
    {
      sanei_scsi_close (s->fd);
      s->fd = -1;
    }

  DBG (11, "<< do_cancel\n");
  return (SANE_STATUS_CANCELLED);
}

static SANE_Status
attach_one (const char *dev)
{
  attach (dev, 0);
  return SANE_STATUS_GOOD;
}

SANE_Status
sane_init (SANE_Int * version_code, SANE_Auth_Callback authorize)
{
  char devnam[PATH_MAX] = "/dev/scanner";
  FILE *fp;

  DBG_INIT ();
  DBG (11, ">> sane_init\n");

#if defined PACKAGE && defined VERSION
  DBG (2, "sane_init: " PACKAGE " " VERSION "\n");
#endif

  if (version_code)
    *version_code = SANE_VERSION_CODE (V_MAJOR, V_MINOR, 0);

  fp = sanei_config_open( CANON_CONFIG_FILE);
  if (fp)
    {
      char line[PATH_MAX];
      size_t len;

      /* read config file */
      while (fgets (line, sizeof (line), fp))
	{
	  if (line[0] == '#')		/* ignore line comments */
            continue;
	  len = strlen (line);
	  if (line[len - 1] == '\n')
            line[--len] = '\0';
  
	  if (!len)
            continue;			/* ignore empty lines */
	  strcpy (devnam, line);
	}
      fclose (fp);
    }
  sanei_config_attach_matching_devices (devnam, attach_one);
  DBG (11, "<< sane_init\n");
  return SANE_STATUS_GOOD;
}

void
sane_exit (void)
{
  CANON_Device *dev, *next;
  DBG (11, ">> sane_exit\n");

  for (dev = first_dev; dev; dev = next)
    {
      next = dev->next;
      free ((void *) dev->sane.name);
      free ((void *) dev->sane.model);
      free (dev);
    }

  DBG (11, "<< sane_exit\n");
}

SANE_Status
sane_get_devices (const SANE_Device *** device_list, SANE_Bool local_only)
{
  static const SANE_Device **devlist = 0;
  CANON_Device *dev;
  int i;
  DBG (11, ">> sane_get_devices\n");

  if (devlist)
    free (devlist);
  devlist = malloc ((num_devices + 1) * sizeof (devlist[0]));
  if (!devlist)
    return (SANE_STATUS_NO_MEM);

  i = 0;
  for (dev = first_dev; dev; dev = dev->next)
    devlist[i++] = &dev->sane;
  devlist[i++] = 0;

  *device_list = devlist;

  DBG (11, "<< sane_get_devices\n");
  return SANE_STATUS_GOOD;
}

SANE_Status
sane_open (SANE_String_Const devnam, SANE_Handle * handle)
{
  SANE_Status status;
  CANON_Device *dev;
  CANON_Scanner *s;
  DBG (11, ">> sane_open\n");

  if (devnam[0] == '\0')
    {
      for (dev = first_dev; dev; dev = dev->next)
	{
	  if (strcmp (dev->sane.name, devnam) == 0)
	    break;
	}

      if (!dev)
	{
	  status = attach (devnam, &dev);
	  if (status != SANE_STATUS_GOOD)
	    return (status);
	}
    }
  else
    {
      dev = first_dev;
    }

  if (!dev)
    return (SANE_STATUS_INVAL);

  s = malloc (sizeof (*s));
  if (!s)
    return SANE_STATUS_NO_MEM;
  memset (s, 0, sizeof (*s));

  s->fd = -1;
  s->hw = dev;

  init_options (s);

  s->next = first_handle;
  first_handle = s;

  *handle = s;

  DBG (11, "<< sane_open\n");
  return SANE_STATUS_GOOD;
}

void
sane_close (SANE_Handle handle)
{
  CANON_Scanner *s = (CANON_Scanner *) handle;
  DBG (11, ">> sane_close\n");

  if (s->fd != -1)
    sanei_scsi_close (s->fd);
  free (s);

  DBG (11, ">> sane_close\n");
}

const SANE_Option_Descriptor *
sane_get_option_descriptor (SANE_Handle handle, SANE_Int option)
{
  CANON_Scanner *s = handle;
  DBG (11, ">> sane_get_option_descriptor\n");

  if ((unsigned) option >= NUM_OPTIONS)
    return (0);

  DBG (11, "<< sane_get_option_descriptor\n");
  return (s->opt + option);
}

SANE_Status
sane_control_option (SANE_Handle handle, SANE_Int option,
		     SANE_Action action, void *val, SANE_Int * info)
{
  CANON_Scanner *s = handle;
  SANE_Status status;
  SANE_Word cap;
  DBG (11, ">> sane_control_option\n");

  if (info)
    *info = 0;

  if (s->scanning)
    return (SANE_STATUS_DEVICE_BUSY);
  if (option >= NUM_OPTIONS)
    return (SANE_STATUS_INVAL);

  cap = s->opt[option].cap;
  if (!SANE_OPTION_IS_ACTIVE (cap))
    return (SANE_STATUS_INVAL);

  if (action == SANE_ACTION_GET_VALUE)
    {
      switch (option)
	{
	  /* word options: */
	case OPT_X_RESOLUTION:
	case OPT_Y_RESOLUTION:
	case OPT_TL_X:
	case OPT_TL_Y:
	case OPT_BR_X:
	case OPT_BR_Y:
	case OPT_NUM_OPTS:
	case OPT_BRIGHTNESS:
	case OPT_CONTRAST:
	case OPT_THRESHOLD:
	  *(SANE_Word *) val = s->val[option].w;
	  if (info)
	    *info |= SANE_INFO_RELOAD_PARAMS;
	  return (SANE_STATUS_GOOD);

	  /* string options: */
	case OPT_MODE:
	  strcpy (val, s->val[option].s);
	  if (info)
	    *info |= SANE_INFO_RELOAD_PARAMS;
	  return (SANE_STATUS_GOOD);
	}
    }
  else if (action == SANE_ACTION_SET_VALUE)
    {
      if (!SANE_OPTION_IS_SETTABLE (cap))
	return (SANE_STATUS_INVAL);

      status = sanei_constrain_value (s->opt + option, val, info);
      if (status != SANE_STATUS_GOOD)
	return status;

      switch (option)
	{
	  /* (mostly) side-effect-free word options: */
	case OPT_X_RESOLUTION:
	case OPT_Y_RESOLUTION:
	case OPT_TL_X:
	case OPT_TL_Y:
	case OPT_BR_X:
	case OPT_BR_Y:
	  if (info && s->val[option].w != *(SANE_Word *) val)
	    *info |= SANE_INFO_RELOAD_PARAMS;
	  /* fall through */
	case OPT_NUM_OPTS:
	case OPT_BRIGHTNESS:
	case OPT_CONTRAST:
	case OPT_THRESHOLD:
	  s->val[option].w = *(SANE_Word *) val;
	  return (SANE_STATUS_GOOD);

	case OPT_MODE:
	  if (info && strcmp (s->val[option].s, (SANE_String) val))
	    *info |= SANE_INFO_RELOAD_OPTIONS | SANE_INFO_RELOAD_PARAMS;
	  if (s->val[option].s)
	    free (s->val[option].s);
	  s->val[option].s = strdup (val);
	  return (SANE_STATUS_GOOD);
	}
    }

  DBG (11, "<< sane_control_option\n");
  return (SANE_STATUS_INVAL);
}

SANE_Status
sane_get_parameters (SANE_Handle handle, SANE_Parameters * params)
{
  CANON_Scanner *s = handle;
  DBG (11, ">> sane_get_parameters\n");

  if (!s->scanning)
    {
      int width, length, xres, yres;
      const char *mode;

      memset (&s->params, 0, sizeof (s->params));

      width = s->val[OPT_BR_X].w - s->val[OPT_TL_X].w + 1;
      length = s->val[OPT_BR_Y].w - s->val[OPT_TL_Y].w + 1;
      xres = s->val[OPT_X_RESOLUTION].w;
      yres = s->val[OPT_Y_RESOLUTION].w;

      /* make best-effort guess at what parameters will look like once
         scanning starts.  */
      if (xres > 0 && yres > 0 && width > 0 && length > 0)
	{
	  s->params.pixels_per_line = width * xres / s->hw->info.mud;
	  s->params.lines = length * yres / s->hw->info.mud;
	}

      mode = s->val[OPT_MODE].s;
      if (strcmp (mode, "Lineart") == 0 || strcmp (mode, "Halftone") == 0)
	{
	  s->params.format = SANE_FRAME_GRAY;
	  s->params.bytes_per_line = s->params.pixels_per_line / 8;
	  /* workaround rounding problems */
	  s->params.pixels_per_line = s->params.bytes_per_line * 8;
	  s->params.depth = 1;
	}
      else if (strcmp (mode, "Gray") == 0)
	{
	  s->params.format = SANE_FRAME_GRAY;
	  s->params.bytes_per_line = s->params.pixels_per_line;
	  s->params.depth = 8;
	}
      else
	{
	  s->params.format = SANE_FRAME_RGB;
	  s->params.bytes_per_line = 3 * s->params.pixels_per_line;
	  s->params.depth = 8;
	}
      s->params.last_frame = SANE_TRUE;
    }

  if (params)
    *params = s->params;

  DBG (11, "<< sane_get_parameters\n");
  return (SANE_STATUS_GOOD);
}

SANE_Status
sane_start (SANE_Handle handle)
{
  int mode;
  char *mode_str;
  CANON_Scanner *s = handle;
  SANE_Status status;
  unsigned char wbuf[72], dbuf[28];
  size_t buf_size;

  DBG (11, ">> sane_start\n");

  /* First make sure we have a current parameter set.  Some of the
     parameters will be overwritten below, but that's OK.  */
  status = sane_get_parameters (s, 0);
  if (status != SANE_STATUS_GOOD)
    return status;

  status = sanei_scsi_open (s->hw->sane.name, &s->fd, 0, 0);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "open of %s failed: %s\n",
	   s->hw->sane.name, sane_strstatus (status));
      return (status);
    }

  mode_str = s->val[OPT_MODE].s;
  s->xres = s->val[OPT_X_RESOLUTION].w;
  s->yres = s->val[OPT_Y_RESOLUTION].w;
  s->ulx = s->val[OPT_TL_X].w;
  s->uly = s->val[OPT_TL_Y].w;
  s->width = s->val[OPT_BR_X].w - s->val[OPT_TL_X].w + 1;
  s->length = s->val[OPT_BR_Y].w - s->val[OPT_TL_Y].w + 1;
  s->brightness = s->val[OPT_BRIGHTNESS].w;
  s->contrast = s->val[OPT_CONTRAST].w;
  s->threshold = s->val[OPT_THRESHOLD].w;
  s->bpp = s->params.depth;
  if (strcmp (mode_str, "Lineart") == 0)
    {
      mode = 4;
      s->image_composition = 0;
      s->reverse = 0;
    }
  else if (strcmp (mode_str, "Halftone") == 0)
    {
      mode = 4;
      s->image_composition = 1;
      s->reverse = 0;
    }
  else if (strcmp (mode_str, "Gray") == 0)
    {
      mode = 5;
      s->image_composition = 2;
      s->reverse = 1;
    }
  else if (strcmp (mode_str, "Color") == 0)
    {
      mode = 6;
      s->image_composition = 5;
      s->reverse = 1;
    }

  memset (wbuf, 0, sizeof (wbuf));
  wbuf[7] = 64;
  wbuf[10] = s->xres >> 8;
  wbuf[11] = s->xres;
  wbuf[12] = s->yres >> 8;
  wbuf[13] = s->yres;
  wbuf[14] = s->ulx >> 24;
  wbuf[15] = s->ulx >> 16;
  wbuf[16] = s->ulx >> 8;
  wbuf[17] = s->ulx;
  wbuf[18] = s->uly >> 24;
  wbuf[19] = s->uly >> 16;
  wbuf[20] = s->uly >> 8;
  wbuf[21] = s->uly;
  wbuf[22] = s->width >> 24;
  wbuf[23] = s->width >> 16;
  wbuf[24] = s->width >> 8;
  wbuf[25] = s->width;
  wbuf[26] = s->length >> 24;
  wbuf[27] = s->length >> 16;
  wbuf[28] = s->length >> 8;
  wbuf[29] = s->length;
  wbuf[30] = s->brightness;
  wbuf[31] = s->threshold;
  wbuf[32] = s->contrast;
  wbuf[33] = s->image_composition;
  wbuf[34] = s->bpp;
  wbuf[36] = 1;
  wbuf[37] = (s->reverse * 128) + 3;
  wbuf[54] = 2;
  wbuf[57] = 1;
  wbuf[58] = 1;

  buf_size = sizeof (wbuf);
  status = set_window (s->fd, wbuf);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "SET WINDOW failed: %s\n", sane_strstatus (status));
      return (status);
    }

  buf_size = sizeof (wbuf);
  memset (wbuf, 0, buf_size);
  status = get_window (s->fd, wbuf, &buf_size);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "GET WINDOW failed: %s\n", sane_strstatus (status));
      return (status);
    }
  DBG (5, "xres=%d\n", (wbuf[10] * 256) + wbuf[11]);
  DBG (5, "yres=%d\n", (wbuf[12] * 256) + wbuf[13]);
  DBG (5, "ulx=%d\n", (wbuf[14] * 256 * 256 * 256)
       + (wbuf[15] * 256 * 256) + (wbuf[16] * 256) + wbuf[17]);
  DBG (5, "uly=%d\n", (wbuf[18] * 256 * 256 * 256)
       + (wbuf[19] * 256 * 256) + (wbuf[20] * 256) + wbuf[21]);
  DBG (5, "width=%d\n", (wbuf[22] * 256 * 256 * 256)
       + (wbuf[23] * 256 * 256) + (wbuf[24] * 256) + wbuf[25]);
  DBG (5, "length=%d\n", (wbuf[26] * 256 * 256 * 256)
       + (wbuf[27] * 256 * 256) + (wbuf[28] * 256) + wbuf[29]);

  status = scan (s->fd);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "start of scan failed: %s\n", sane_strstatus (status));
      return (status);
    }

  memset (dbuf, 0, sizeof (dbuf));
  buf_size = sizeof (dbuf);
  status = get_data_status (s->fd, dbuf, &buf_size);
  if (status != SANE_STATUS_GOOD)
    {
      DBG (1, "GET DATA STATUS failed: %s\n", sane_strstatus (status));
      return (status);
    }
  DBG (5, "Magnified Width=%d\n", (dbuf[12] * 256 * 256 * 256)
       + (dbuf[13] * 256 * 256) + (dbuf[14] * 256) + dbuf[15]);
  DBG (5, "Magnified Length=%d\n", (dbuf[16] * 256 * 256 * 256)
       + (dbuf[17] * 256 * 256) + (dbuf[18] * 256) + dbuf[19]);
  DBG (5, "Data=%d bytes\n", (dbuf[20] * 256 * 256 * 256)
       + (dbuf[21] * 256 * 256) + (dbuf[22] * 256) + dbuf[23]);

  s->bytes_to_read = s->params.bytes_per_line * s->params.lines;

  DBG (1, "%d pixels per line, %d bytes, %d lines high, total %lu bytes, "
       "dpi=%d\n", s->params.pixels_per_line, s->params.bytes_per_line,
       s->params.lines, (u_long) s->bytes_to_read, s->val[OPT_Y_RESOLUTION].w);

  s->scanning = SANE_TRUE;

  DBG (11, "<< sane_start\n");
  return (SANE_STATUS_GOOD);
}

SANE_Status
sane_read (SANE_Handle handle, SANE_Byte * buf, SANE_Int max_len,
	   SANE_Int * len)
{
  CANON_Scanner *s = handle;
  SANE_Status status;
  size_t nread;
  DBG (11, ">> sane_read\n");

  *len = 0;

  if (s->bytes_to_read == 0)
    {
      do_cancel (s);
      return (SANE_STATUS_EOF);
    }

  if (!s->scanning)
    return (do_cancel (s));

  nread = max_len;
  if (nread > s->bytes_to_read)
    nread = s->bytes_to_read;

  status = read_data (s->fd, buf, &nread);
  if (status != SANE_STATUS_GOOD)
    {
      do_cancel (s);
      return (SANE_STATUS_IO_ERROR);
    }
  *len = nread;
  s->bytes_to_read -= nread;

  DBG (11, "<< sane_read\n");
  return (SANE_STATUS_GOOD);
}

void
sane_cancel (SANE_Handle handle)
{
  CANON_Scanner *s = handle;
  DBG (11, ">> sane_cancel\n");

  s->scanning = SANE_FALSE;

  DBG (11, "<< sane_cancel\n");
}

SANE_Status
sane_set_io_mode (SANE_Handle handle, SANE_Bool non_blocking)
{
  DBG (5, ">> sane_set_io_mode\n");
  DBG (5, "<< sane_set_io_mode\n");

  return SANE_STATUS_UNSUPPORTED;
}

SANE_Status
sane_get_select_fd (SANE_Handle handle, SANE_Int * fd)
{
  DBG (5, ">> sane_get_select_fd\n");
  DBG (5, "<< sane_get_select_fd\n");

  return SANE_STATUS_UNSUPPORTED;
}
