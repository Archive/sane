.TH sane-hp 5 "28 October 1998"
.IX sane-hp
.SH NAME
sane-hp - SANE backend for HP ScanJet scanners
.SH DESCRIPTION
The
.B sane-hp
library implements a SANE (Scanner Access Now Easy) backend that
provides access to HP ScanJet scanners.  At present, the following
scanners are known positively to work with this backend:
.PP
.RS
Model:        Product id:
.br
----------    -----------
.br
ScanJet IIcx  C2500A 3332
.br
ScanJet IIp   C1790A
.br
ScanJet 3C    C2520A 3503
.br
ScanJet 4C    C2520A
.br
HP 6100       C2520A 3644
.br
HP 6200       ??????
.br
ScanJet 3P    C2570A 3406
.br
ScanJet 4P    C1130A 3540
.br
PhotoSmart    C5100A
.br
ScanJet 5P    C5110A
.RE
.PP
The PhotoSmart scanner support seems to have some problems (reported by
Peter Kirchgessner <Pkirchg@aol.com>):
.TP
o Scanning photos (paper): supported from 75 to 300 dpi.
Problems: (1) aquire preview moves the photo into the scanner.
Start scan then scans while moving the photo out of the
scanner. The image is mirrored top to bottom.
This problem also occurs when scanning slides/negatives.
Would be nice to be able to mirror the image when reading it in.
(2) Halftone and lineart scanning not supported (scanner firmware?).
The output file out.pnm is a true-color file with wrong header
(P4 instead of P6).
(3) Setting the scan geometry length to a value larger than the
current photo, the bottom of the image is filled with rubbish.
.TP
o Scanning slides: supported from 100 to 2400 dpi.
Problems: (1) Scanning at less than 300 dpi keeps the image length fixed
while the image width is scaled (scanner firmware?). I dont care
about that.  Noone will use that. (2) Halftone/Lineart scanning not
supported (scanner firmware?).
.TP
o Scanning negatives: supported from 100 to 2400 dpi.
Problems: (1) When scanning strips with 5 negatives of size 24x36mm,
the 5th image shows wrong colours (scanner firmware?).
(2) Halftone/Lineart scanning not supported (scanner firmware?).
(3) Would be nice to show the images inverse.
.TP
o General problems.
(1) When changing the scanner between slide/negative/photo-mode,
xscanimage must be restarted to show the possible scan resolutions
and scan geometries. (2) The Windows 95 software that comes with
the scanner has two advantages: (a) show negatives as positives
(b) quite good automatic adjustment of brightness/contrast
.PP
If you own a ScanJet scanner other than the ones listed above, please
let us know if your model works with this backend.  To do this, send a
mail with the relevant information for your scanner to
.IR sane\-devel@mostang.com .
.SH "DEVICE NAMES"
This backend expects device names of the form:
.PP
.RS
.I special
.RE
.PP
Where
.I special
is the UNIX path-name for the special device that corresponds to the
scanner.  The special device name must be a generic SCSI device or a
symlink to such a device.  Under Linux, such a device name could be
.I /dev/sga
or
.IR /dev/sge ,
for example.
.SH CONFIGURATION
The contents of the
.I hp.conf
file is a list of device names that correspond to HP ScanJet
scanners.  Empty lines and lines starting with a hash mark (#) are
ignored.  A sample configuration file is shown below:
.PP
.RS
/dev/scanner
.br
# this is a comment
.br
/dev/sge
.RE
.SH FILES
.TP
.I @CONFIGDIR@/hp.conf
The backend configuration file (see also description of
.B SANE_CONFIG_DIR
below).
.TP
.I @LIBDIR@/libsane-hp.a
The static library implementing this backend.
.TP
.I @LIBDIR@/libsane-hp.so
The shared library implementing this backend (present on systems that
support dynamic loading).
.SH ENVIRONMENT
.TP
.B SANE_CONFIG_DIR
This environment variable specifies the list of directories that may
contain the configuration file.  Under UNIX, the directories are
separated by a colon (`:'), under OS/2, they are separated by a
semi-colon (`;').  If this variable is not set, the configuration file
is searched in two default directories: first, the current working
directory (".") and then in @CONFIGDIR@.  If the value of the
environment variable ends with the directory separator character, then
the default directories are searched after the explicitly specified
directories.  For example, setting
.B SANE_CONFIG_DIR
to "/tmp/config:" would result in directories "tmp/config", ".", and
"@CONFIGDIR@" being searched (in this order).
.TP
.B SANE_DEBUG_HP
If the library was compiled with debug support enabled, this
environment variable controls the debug level for this backend.  E.g.,
a value of 128 requests all debug output to be printed.  Smaller
levels reduce verbosity.
.SH "SEE ALSO"
sane\-scsi(5)
.SH AUTHOR
David Mosberger
.PP
The backend is derived from
.BR hpscanpbm (1)
(see sources for copyright).
.SH BUGS
This is a minimal backend.  E.g., at present there is no support for
down-loadable tone maps.
