/* sane - Scanner Access Now Easy.
   Copyright (C) 1997 BYTEC GmbH Germany
   Written by Helmut Koeberle, Email: helmut.koeberle@bytec.de

   This file is part of the SANE package.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

   As a special exception, the authors of SANE give permission for
   additional uses of the libraries contained in this release of SANE.

   The exception is that, if you link a SANE library with other files
   to produce an executable, this does not by itself cause the
   resulting executable to be covered by the GNU General Public
   License.  Your use of that executable is in no way restricted on
   account of linking the SANE library code into it.

   This exception does not, however, invalidate any other reasons why
   the executable file might be covered by the GNU General Public
   License.

   If you submit changes to SANE to the maintainers to be included in
   a subsequent release, you agree by submitting the changes that
   those changes may be distributed with this exception intact.

   If you write modifications of your own for SANE, it is your choice
   whether to permit this exception to apply to your modifications.
   If you do not wish that, delete this exception notice. */

/* This file implements the low-level scsi-commands.  */

static SANE_Status
test_unit_ready (int fd)
{
  static u_char cmd[6];
  int status;
  DBG (11, ">> test_unit_ready\n");

  memset (cmd, 0, sizeof (cmd));
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< test_unit_ready\n");
  return (status);
}

#if 0
static SANE_Status
request_sense (int fd, void *buf, size_t * buf_size)
{
  static u_char cmd[6];
  int status;
  DBG (11, ">> request_sense\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x03;
  cmd[4] = 14;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), buf, buf_size);

  DBG (11, "<< request_sense\n");
  return (status);
}
#endif

static SANE_Status
inquiry (int fd, int evpd, void *buf, size_t * buf_size)
{
  static u_char cmd[6];
  int status;
  DBG (11, ">> inquiry\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x12;
  cmd[1] = evpd;
  cmd[2] = evpd ? 0xf0 : 0;
  cmd[4] = evpd ? 74 : 36;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), buf, buf_size);

  DBG (11, "<< inquiry\n");
  return (status);
}

#if 0
static SANE_Status
mode_select (int fd)
{

  static u_char cmd[6 + 12];
  int status;
  DBG (11, ">> mode_select\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x15;
  cmd[1] = 16;
  cmd[4] = 12;
  cmd[6 + 4] = 3;
  cmd[6 + 5] = 6;
  cmd[6 + 8] = 0x02;
  cmd[6 + 9] = 0x58;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< mode_select\n");
  return (status);
}
#endif

#if 0
static SANE_Status
reserve_unit (int fd)
{

  static u_char cmd[6];
  int status;
  DBG (11, ">> reserve_unit\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x16;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< reserve_unit\n");
  return (status);
}
#endif

#if 0
static SANE_Status
release_unit (int fd)
{

  static u_char cmd[6];
  int status;
  DBG (11, ">> release_unit\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x17;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< release_unit\n");
  return (status);
}
#endif

static SANE_Status
mode_sense (int fd, void *buf, size_t * buf_size)
{

  static u_char cmd[6];
  int status;
  DBG (11, ">> mode_sense\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x1a;
  cmd[2] = 3;
  cmd[4] = 12;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), buf, buf_size);

  DBG (11, "<< mode_sense\n");
  return (status);
}

static SANE_Status
scan (int fd)
{
  static u_char cmd[6 + 1];
  int status;
  DBG (11, ">> scan\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x1b;
  cmd[4] = 1;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< scan\n");
  return (status);
}

#if 0
static SANE_Status
send_diagnostics (int fd)
{
  static u_char cmd[6];
  int status;
  DBG (11, ">> send_diagnostics\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x1d;
  cmd[1] = 4;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< send_diagnostics\n");
  return (status);
}
#endif

static SANE_Status
set_window (int fd, void *data)
{

  static u_char cmd[10 + 72];
  int status;
  DBG (11, ">> set_window\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x24;
  cmd[8] = 72;
  memcpy (cmd + 10, data, 72);
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< set_window\n");
  return (status);

}

static SANE_Status
get_window (int fd, void *buf, size_t * buf_size)
{

  static u_char cmd[10];
  int status;
  DBG (11, ">> get_window\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x25;
  cmd[1] = 1;
  cmd[8] = 72;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), buf, buf_size);

  DBG (11, "<< get_window\n");
  return (status);
}

static SANE_Status
read_data (int fd, void *buf, size_t * buf_size)
{
  static u_char cmd[10];
  int status;
  DBG (11, ">> read_data\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x28;
  cmd[6] = *buf_size >> 16;
  cmd[7] = *buf_size >> 8;
  cmd[8] = *buf_size;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), buf, buf_size);

  DBG (11, "<< read_data\n");
  return (status);
}

static SANE_Status
medium_position (int fd)
{
  static u_char cmd[10];
  int status;
  DBG (11, ">> medium_psosition\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x31;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), 0, 0);

  DBG (11, "<< medium_psosition\n");
  return (status);
}

static SANE_Status
get_data_status (int fd, void *buf, size_t * buf_size)
{
  static u_char cmd[10];
  int status;
  DBG (11, ">> get_data_status\n");

  memset (cmd, 0, sizeof (cmd));
  cmd[0] = 0x34;
  cmd[8] = 28;
  status = sanei_scsi_cmd (fd, cmd, sizeof (cmd), buf, buf_size);

  DBG (11, "<< get_data_status\n");
  return (status);
}
