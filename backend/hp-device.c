/* sane - Scanner Access Now Easy.
   Copyright (C) 1997 Geoffrey T. Dairiki
   This file is part of the SANE package.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

   As a special exception, the authors of SANE give permission for
   additional uses of the libraries contained in this release of SANE.

   The exception is that, if you link a SANE library with other files
   to produce an executable, this does not by itself cause the
   resulting executable to be covered by the GNU General Public
   License.  Your use of that executable is in no way restricted on
   account of linking the SANE library code into it.

   This exception does not, however, invalidate any other reasons why
   the executable file might be covered by the GNU General Public
   License.

   If you submit changes to SANE to the maintainers to be included in
   a subsequent release, you agree by submitting the changes that
   those changes may be distributed with this exception intact.

   If you write modifications of your own for SANE, it is your choice
   whether to permit this exception to apply to your modifications.
   If you do not wish that, delete this exception notice.

   This file is part of a SANE backend for HP Scanners supporting
   HP Scanner Control Language (SCL).
*/

#include <sane/config.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "hp-device.h"
#include "hp-accessor.h"
#include "hp-option.h"
#include "hp-scsi.h"
#include "hp-scl.h"

SANE_Status
sanei_hp_device_probe (enum hp_device_compat_e *compat, HpScsi scsi)
{
  static struct {
      HpScl		cmd;
      const char *	model;
      enum hp_device_compat_e	flag;
  }	probes[] = {
      { SCL_HP_MODEL_2, "IIc",   HP_COMPAT_2C },
      { SCL_HP_MODEL_3, "IIp",   HP_COMPAT_2P },
      { SCL_HP_MODEL_4, "IIcx",  HP_COMPAT_2CX },
      { SCL_HP_MODEL_5, "4c/3c", HP_COMPAT_4C },
      { SCL_HP_MODEL_6, "3p",    HP_COMPAT_3P },
      { SCL_HP_MODEL_8, "4P",    HP_COMPAT_4P },
      { SCL_HP_MODEL_9, "5P",    HP_COMPAT_5P },
      { SCL_HP_MODEL_10, "Photo Scanner",    HP_COMPAT_PS },
      { SCL_HP_MODEL_14, "6200C",    HP_COMPAT_6200C }
  };
  int		i;
  char		buf[8];
  size_t	len;
  SANE_Status	status;
  static char	*last_device = NULL;
  static enum hp_device_compat_e last_compat;

  assert(scsi);
  DBG(1, "probe_scanner: Probing %s\n", sanei_hp_scsi_devicename (scsi));

  if (last_device != NULL)  /* Look if we already probed the device */
  {
    if (strcmp (last_device, sanei_hp_scsi_devicename (scsi)) == 0)
    {
      DBG(3, "probe_scanner: use cached compatibility flags\n");
      *compat = last_compat;
      return SANE_STATUS_GOOD;
    }
    sanei_hp_free (last_device);
    last_device = NULL;
  }
  *compat = 0;
  for (i = 0; i < sizeof(probes)/sizeof(probes[0]); i++)
    {
      len = sizeof(buf);
      if (!FAILED( status = sanei_hp_scl_upload(scsi, probes[i].cmd,
					  buf, sizeof(buf)) ))
	{
	  DBG(1, "probe_scanner: Scanjet %s compatible\n", probes[i].model);
	  *compat |= probes[i].flag;
	}
      else if (!UNSUPPORTED( status ))
	  return status;	/* SCL inquiry failed */
    }
  /* Save values for next call */
  last_device = sanei_hp_strdup (sanei_hp_scsi_devicename (scsi));
  last_compat = *compat;

  return SANE_STATUS_GOOD;
}

hp_bool_t
sanei_hp_device_compat (HpDevice this, enum hp_device_compat_e which)
{
  return (this->compat & which) != 0;
}

SANE_Status
sanei_hp_device_new (HpDevice * newp, const char * devname)
{
  HpDevice	this;
  HpScsi	scsi;
  SANE_Status	status;
  char *	str;

  if (FAILED( sanei_hp_scsi_new(&scsi, devname) ))
    {
      DBG(1, "%s: Can't open scsi device\n", devname);
      return SANE_STATUS_INVAL;	/* Can't open device */
    }

  if (sanei_hp_scsi_inq(scsi)[0] != 0x03
      || memcmp(sanei_hp_scsi_vendor(scsi), "HP      ", 8) != 0)
    {
      DBG(1, "%s: does not seem to be an HP scanner\n", devname);
      sanei_hp_scsi_destroy(scsi);
      return SANE_STATUS_INVAL;
    }

  /* reset scanner; returns all parameters to defaults */
  if (FAILED( sanei_hp_scl_reset(scsi) ))
    {
      DBG(1, "sanei_hp_device_new: SCL reset failed\n");
      sanei_hp_scsi_destroy(scsi);
      return SANE_STATUS_IO_ERROR;
    }

  /* Things seem okay, allocate new device */
  this = sanei_hp_allocz(sizeof(*this));
  this->data = sanei_hp_data_new();

  if (!this || !this->data)
      return SANE_STATUS_NO_MEM;

  this->sanedev.name = sanei_hp_strdup(devname);
  str = sanei_hp_strdup(sanei_hp_scsi_model(scsi));
  if (!this->sanedev.name || !str)
      return SANE_STATUS_NO_MEM;
  this->sanedev.model = str;
  if ((str = strchr(str, ' ')) != 0)
      *str = '\0';
  this->sanedev.vendor = "Hewlett-Packard";
  this->sanedev.type   = "flatbed scanner";

  status = sanei_hp_device_probe(&(this->compat), scsi);
  if (!FAILED(status))
      status = sanei_hp_optset_new(&this->options, scsi, this);
  sanei_hp_scsi_destroy(scsi);

  if (FAILED(status))
    {
      DBG(1, "sanei_hp_device_new: %s: probe failed (%s)\n",
	  devname, sane_strstatus(status));
      sanei_hp_data_destroy(this->data);
      sanei_hp_free((void *)this->sanedev.name);
      sanei_hp_free((void *)this->sanedev.model);
      sanei_hp_free(this);
      return status;
    }

  DBG(1, "sanei_hp_device_new: %s: found HP ScanJet model %s\n",
      devname, this->sanedev.model);

  *newp = this;
  return SANE_STATUS_GOOD;
}

const SANE_Device *
sanei_hp_device_sanedevice (HpDevice this)
{
  return &this->sanedev;
}

