.TH scanimage 1 "13 May 1998"
.IX scanimage
.SH NAME
scanimage - scan an image
.SH SYNOPSIS
.B scanimage
.RB [ -d | --device-name
.IR dev ]
.RB [ -h | --help ]
.RB [ -L | --list-devices ]
.RB [ -v | --verbose ]
.RB [ -V | --version ]
.RI [ device-specific-options ]
.SH DESCRIPTION
.B scanimage
is a command-line interface to control image acquisition devices such
as flatbed scanners or cameras.  The device is controlled via
command-line options.  After command-line processing,
.B scanimage
normally proceeds to acquire an image.  The image data is written to
standard output in one of the PNM (portable aNyMaP) formats (PBM for
black-and-white images, PGM for grayscale images, and PPM for color
images).
.B scanimage
accesses image acquisition devices through the SANE (Scanner Access
Now Easy) interface and can thus support any device for which there
exists a SANE backend (try "apropos sane\-" to get a list of available
backends).
.SH OPTIONS
.PP
The
.B -d
or
.B --device-name
options must be followed by a SANE device-name.  A (partial) list of
available devices can be obtained with the
.B --list-devices
option (see below).  If no device-name is specified explicitly,
.B scanimage
will attempt to open the first available device.
.PP
The
.B -h
or
.B --help
options request help information.  The information is printed on
standard output and in this case, no attempt will be made to acquire
an image.
.PP
The
.B -L
or
.B --list-devices
option requests a (partial) list of devices that are available.  The
list is not complete since some devices may be available, but are not
listed in any of the configuration files (which are typically stored
in directory @CONFIGDIR@).  This is particularly the case when
accessing scanners through the network.  If a device is not listed in
a configuration file, the only way to access it is by its full device
name.  You may need to consult your system administrator to find out
the names of such devices.

The
.B -v
or
.B --verbose
options increase the verbosity of the operation of
.B scanimage.
The option may be specified repeatedly, each time increasing the verbosity
level.

The
.B -V
or
.B --version
option requests that
.B scanimage
print the program and package name, as well as the version number of
the SANE distribution that it came with.

As you might imagine, much of the power of
.B scanimage
comes from the fact that it can control any SANE backend.  Thus, the
exact set of command-line options depends on the capabilities of the
selected device.  To see the options for a device named
.IR dev ,
invoke
.B scanimage
via a command-line of the form:
.PP
.RS
scanimage --help --device
.I dev
.RE
.PP
The documentation for the device-specific options printed by
.B --help
is best explained with a few examples:

 --brightness -100..100% [0]
.br
    Controls the brightness of the acquired image.
.PP
.RS
The description above shows that option
.B --brightness
expects an option value in the range from -100 to 100 percent.  The
value in square brackets indicates that the current option value is 0
percent.
.RE

 --default-enhancements
.br
    Set default values for enhancement controls.
.PP
.RS
The description above shows that option
.B --default-enhancements
has no option value.  It should be thought of as having an immediate
effect at the point of the command-line at which it appears.  For
example, since this option resets the
.B --brightness
option, the option-pair
.B --brightness 50 --default-enhancements
would effectively be a no-op.
.RE

 --mode Lineart|Gray|Color [Gray]
.br
    Selects the scan mode (e.g., lineart or color).
.PP
.RS
The description above shows that option
.B --mode
accepts an argument that must be one of the strings
.BR Lineart ,
.BR Gray ,
or
.BR Color .
The value in the square bracket indicates that the option is currently
set to
.BR Gray .
For convenience, it is legal to abbreviate the string values as long as
they remain unique.  Also, the case of the spelling doesn't matter.  For
example, option setting
.B --mode col
is identical to
.BR "--mode Color" .
.RE

 --custom-gamma[=(yes|no)] [inactive]
.br
    Determines whether a builtin or a custom gamma-table
.br
    should be used.
.PP
.RS
The description above shows that option
.B --custom-gamma
expects either no option value, a "yes" string, or a "no" string.
Specifying the option with no value is equivalent to specifying "yes".
The value in square-brackets indicates that the option is not
currently active.  That is, attempting to set the option would result
in an error message.  The set of available options typically depends
on the settings of other options.  For example, the
.B --custom-gamma
table might be active only when a grayscale or color scan-mode has
been requested.

Note that the
.B --help
option is processed only after all other options have been processed.
This makes it possible to see the option settings for a particular
mode by specifying the appropriate mode-options along
with the
.B --help
option.  For example, the command-line:
.PP
  scanimage --help --mode color
.PP
would print the option settings that are in effect when the color-mode
is selected.
.RE

 --gamma-table 0..255,...
.br
    Gamma-correction table.  In color mode this option
.br
    equally affects the red, green, and blue channels
.br
    simultaneously (i.e., it is an intensity gamma table).
.PP
.RS
The description above shows that option
.B --gamma-table
expects zero or more values in the range 0 to 255.  For example, a
legal value for this option would be "3,4,5,6,7,8,9,10,11,12".  Since
it's cumbersome to specify long vectors in this form, the same can be
expressed by the abbreviated form "[0]3-[9]12".  What this means is
that the first vector element is set to 0, the 9-th element is set to
12 and the values inbetween are interpolated linearly.  Of course, it
is possible to specify multiple such linear segments.  For example,
"[0]3-[2]3-[6]7,[7]10-[9]6" is equivalent to "3,3,3,4,5,6,7,10,8,6".
.RE

.br
 --filename <string> [/tmp/input.ppm]
.br
    The filename of the image to be loaded.
.PP
.RS
The descriptoin above is an example of an option that takes an
arbitrary string value (which happens to be a filename).  Again,
the value in brackets show that the option is current set to the
filename 
.BR /tmp/input.ppm .
.RE

.SH FILES
.TP
.I @CONFIGDIR@
This directory holds various configuration files.  For details, please
refer to the manual pages listed below.
.SH "SEE ALSO"
xscanimage(1), xcam(1), sane\-dll(5), sane\-dmc(5), sane\-epson(5),
sane\-hp(5), sane\-microtek(5), sane\-mustek(5), sane\-net(5), sane\-pnm(5),
sane\-pint(5), sane\-qcam(5), sane\-umax(5)
.SH AUTHOR
David Mosberger, Andreas Beck, and Gordon Matzigkeit
.SH BUGS
For vector options, the help output currently has no indication as to
how many elements a vector-value should have.
