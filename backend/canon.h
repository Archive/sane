/* sane - Scanner Access Now Easy.
   Copyright (C) 1997 BYTEC GmbH Germany
   Written by Helmut Koeberle, Email: helmut.koeberle@bytec.de

   This file is part of the SANE package.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA.

   As a special exception, the authors of SANE give permission for
   additional uses of the libraries contained in this release of SANE.

   The exception is that, if you link a SANE library with other files
   to produce an executable, this does not by itself cause the
   resulting executable to be covered by the GNU General Public
   License.  Your use of that executable is in no way restricted on
   account of linking the SANE library code into it.

   This exception does not, however, invalidate any other reasons why
   the executable file might be covered by the GNU General Public
   License.

   If you submit changes to SANE to the maintainers to be included in
   a subsequent release, you agree by submitting the changes that
   those changes may be distributed with this exception intact.

   If you write modifications of your own for SANE, it is your choice
   whether to permit this exception to apply to your modifications.
   If you do not wish that, delete this exception notice. */

#ifndef canon_h
#define canon_h 1

#include <sys/types.h>

typedef enum
  {
    OPT_NUM_OPTS = 0,

    OPT_MODE_GROUP,
    OPT_MODE,
    OPT_X_RESOLUTION,
    OPT_Y_RESOLUTION,

    OPT_GEOMETRY_GROUP,
    OPT_TL_X,			/* top-left x */
    OPT_TL_Y,			/* top-left y */
    OPT_BR_X,			/* bottom-right x */
    OPT_BR_Y,			/* bottom-right y */

    OPT_ENHANCEMENT_GROUP,
    OPT_BRIGHTNESS,
    OPT_CONTRAST,
    OPT_THRESHOLD,

    /* must come last: */
    NUM_OPTIONS
  }
CANON_Option;

typedef union
  {
    SANE_Word w;
    SANE_Word *wa;		/* word array */
    SANE_String s;
  }
Option_Value;

typedef struct CANON_Info
  {
    SANE_Range xres_range;
    SANE_Range yres_range;
    SANE_Range x_range;
    SANE_Range y_range;
    SANE_Range brightness_range;
    SANE_Range contrast_range;
    SANE_Range threshold_range;

    SANE_Range x_adf_range;
    SANE_Range y_adf_range;
    SANE_Int xres_default;
    SANE_Int yres_default;
    SANE_Int bmu;
    SANE_Int mud;
  }
CANON_Info;

typedef struct CANON_Device
  {
    struct CANON_Device *next;
    SANE_Device sane;
    CANON_Info info;
  }
CANON_Device;

typedef struct CANON_Scanner
  {
    struct CANON_Scanner *next;
    int fd;
    CANON_Device *hw;
    SANE_Option_Descriptor opt[NUM_OPTIONS];
    Option_Value val[NUM_OPTIONS];
    SANE_Parameters params;

    SANE_Int xres;
    SANE_Int yres;
    SANE_Int ulx;
    SANE_Int uly;
    SANE_Int width;
    SANE_Int length;
    SANE_Int brightness;
    SANE_Int contrast;
    SANE_Int threshold;
    SANE_Int image_composition;
    SANE_Int bpp;
    SANE_Bool reverse;

    size_t bytes_to_read;
    int scanning;
  }
CANON_Scanner;

#endif /* not canon_h */
